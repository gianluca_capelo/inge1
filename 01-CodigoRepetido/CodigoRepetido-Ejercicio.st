!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/9/2021 16:02:33'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| addingCustomer customerBook |
	
	customerBook := CustomerBook new.
	
	addingCustomer := [ customerBook addCustomerNamed: 'John Lennon'].

	"should:notTakeMoreThan: compares with '<=', while we want '<'. Therefore, we use 49 as a collaborator."	
	self should: addingCustomer notTakeMoreThan: 49 milliSeconds.
! !

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/9/2021 16:01:46'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney removingACustomer |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	removingACustomer := [customerBook addCustomerNamed: paulMcCartney].
	
	"should:notTakeMoreThan: compares with '<=', while we want '<'. Therefore, we use 99 as a collaborator."
	self should: removingACustomer notTakeMoreThan: 99 milliSeconds.
	
! !

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/9/2021 16:08:47'!
test03CanNotAddACustomerWithEmptyName 
	
	| customerBook addingCustomerWithEmptyName assertCorrectErrorMessageAndThatCustomerWasNotAdded |
			
	customerBook := CustomerBook new.
	
	addingCustomerWithEmptyName := [customerBook addCustomerNamed: ''].
	assertCorrectErrorMessageAndThatCustomerWasNotAdded := [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ].
		
	self should: addingCustomerWithEmptyName raise: Error withExceptionDo: assertCorrectErrorMessageAndThatCustomerWasNotAdded.! !

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/8/2021 22:47:45'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon assertCustomerWasNotRemoved removingInvalidCustomer |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	removingInvalidCustomer := [customerBook removeCustomerNamed: 'Paul McCartney'].
	assertCustomerWasNotRemoved := [ :anError | self assert: customerBook customersAre: (OrderedCollection with: johnLennon)].
		
	self should: removingInvalidCustomer raise: NotFound withExceptionDo: assertCustomerWasNotRemoved.! !

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/9/2021 16:52:11'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	"customerBook := self initialize: CustomerBook new withSuspendedCustomerNamed: paulMcCartney."
	
	self assert: customerBook numberOfCustomersIs: 1 withActive: 0 withSuspended: 1. 
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/8/2021 22:47:23'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assert: customerBook numberOfCustomersIs: 0 withActive: 0 withSuspended: 0. 
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/9/2021 17:02:13'!
test07CanNotSuspendAnInvalidCustomer

	| customerBook johnLennon assertSameCustomers |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	assertSameCustomers := [ :anError | self assert: customerBook customersAre: (OrderedCollection with: johnLennon)].
		
	self assert: customerBook cantSuspend: 'George Harrison' withExceptionDo: assertSameCustomers.
! !

!CustomerBookTest methodsFor: 'tests' stamp: 'Min 9/9/2021 17:04:19'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon assertSameCustomers |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	assertSameCustomers :=  [ :anError | self assert: customerBook customersAre: (OrderedCollection with: johnLennon)].
		
	self assert: customerBook cantSuspend: johnLennon withExceptionDo: assertSameCustomers.! !


!CustomerBookTest methodsFor: 'assertions' stamp: 'Min 9/9/2021 17:02:33'!
assert: customerBook cantSuspend: aClientThatCannotBeSuspended withExceptionDo: assertionsOnError

	| invalidSuspension |

	invalidSuspension := [customerBook suspendCustomerNamed: aClientThatCannotBeSuspended].
	
	self should: invalidSuspension raise: CantSuspend withExceptionDo: assertionsOnError.


	! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'Min 9/8/2021 22:06:44'!
assert: aCustomerBook customersAre: anExpectedCollectionOfCustomers

	self assert: aCustomerBook numberOfCustomers = anExpectedCollectionOfCustomers size.
	self assert: (anExpectedCollectionOfCustomers allSatisfy: [:aCustomerName | aCustomerBook includesCustomerNamed: aCustomerName]).

	! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'Min 9/8/2021 22:54:32'!
assert: aCustomerBook numberOfCustomersIs: anExpectedTotal withActive: anExpectedActive withSuspended: anExpectedSuspended

	self assert: anExpectedTotal equals: aCustomerBook numberOfCustomers.
	self assert: anExpectedActive equals: aCustomerBook numberOfActiveCustomers.
	self assert: anExpectedSuspended equals: aCustomerBook numberOfSuspendedCustomers.! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'Min 9/7/2021 21:45:56'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'Min 9/7/2021 21:46:02'!
numberOfCustomers
	
	^self numberOfActiveCustomers + self numberOfSuspendedCustomers.! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'Min 9/7/2021 21:46:13'!
removeCustomerNamed: aName 

	^active remove: aName ifAbsent: [
		suspended remove: aName ifAbsent: [
			NotFound signal
			]
		]! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:13'!
customerAlreadyExistsErrorMessage

	^'customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:09'!
customerCanNotBeEmptyErrorMessage

	^'customer name cannot be empty!!!!!!'! !
