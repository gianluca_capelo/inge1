!classDefinition: #I category: 'Natural Numbers'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'Natural Numbers'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 12:35:27'!
* multiplicand

	^multiplicand! !

!I class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 12:35:36'!
+ adder

	^adder next! !

!I class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 12:35:51'!
- sustrahend

	self error: self negativeNumbersNotSupportedErrorDescription! !

!I class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 12:36:00'!
/ dividend

	dividend dividingWithOneAsDivisor! !


!I class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 12:32:01'!
dividingWithOneAsDivisor
	
	^self! !

!I class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 12:34:37'!
divisionBeingTheDifferencePlusOneBetween: dividend and: divisor

	^I! !

!I class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 12:34:28'!
multiplyByNumberBiggerThanOne: multiplicandBiggerThanOne 

	^multiplicandBiggerThanOne! !

!I class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 12:34:07'!
substractFrom: minuend
	
	^minuend previous! !

!I class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 12:33:58'!
timesDivideTo: dividend

	^dividend! !


!I class methodsFor: 'next & previous' stamp: 'ARM 9/1/2021 13:45:29'!
next

	^II.! !

!I class methodsFor: 'next & previous' stamp: 'ARM 3/19/2018 15:30:11'!
previous

	self error: 'El uno es el primer numero natural'! !


!I class methodsFor: 'error description' stamp: 'ARM 9/1/2021 12:31:08'!
negativeNumbersNotSupportedErrorDescription

	^'In natural numbers, can not substract a number minus a bigger or equal number'! !


!classDefinition: #II category: 'Natural Numbers'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'Natural Numbers'!
II class
	instanceVariableNames: 'previous next'!

!II class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 12:47:56'!
* multiplicand

	^multiplicand multiplyByNumberBiggerThanOne: self! !

!II class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 12:48:07'!
+ adder

	^self previous + adder next! !

!II class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 12:48:38'!
- sustrahend

	^sustrahend substractFrom: self! !

!II class methodsFor: 'arithmetic operations' stamp: 'ARM 9/1/2021 13:31:43'!
/ divisor

	^divisor timesDivideTo: self! !


!II class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 13:30:42'!
dividingWithOneAsDivisor

	self error: self canNotDivideByBiggerNumberErrorDescription! !

!II class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 13:29:20'!
divisionBeingTheDifferencePlusOneBetween: dividendo and: divisor

	^[(dividendo - divisor) / divisor + I] on: Error do: [I]! !

!II class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 13:30:33'!
multiplyByNumberBiggerThanOne: multiplierBiggerThanOne

	^multiplierBiggerThanOne + (self previous * multiplierBiggerThanOne)! !

!II class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 12:44:59'!
substractFrom: sustrahend
 
	^sustrahend previous - self previous	! !

!II class methodsFor: 'auxiliar operations' stamp: 'ARM 9/1/2021 13:36:03'!
timesDivideTo: dividend

	^[(dividend + I - self) divisionBeingTheDifferencePlusOneBetween: dividend and: self]
		on: Error
		do: [self error: self canNotDivideByBiggerNumberErrorDescription]! !


!II class methodsFor: 'next & previous' stamp: 'ARM 4/6/2021 14:36:27'!
next

	next ifNotNil: [^next].
	next := II createChildNamed: (self name, 'I').
	next previous: self.
	
	^next
! !

!II class methodsFor: 'next & previous' stamp: 'ARM 3/19/2018 15:38:06'!
previous

	^previous! !

!II class methodsFor: 'next & previous' stamp: 'ARM 9/1/2021 13:46:55'!
previous: aNumber 
	
	previous _ aNumber! !


!II class methodsFor: 'remove all' stamp: 'ARM 3/20/2018 11:29:48'!
removeAllNext

	next ifNotNil:
	[
		next removeAllNext.
		next removeFromSystem.
		next := nil.
	]
! !


!II class methodsFor: 'error description' stamp: 'ARM 9/1/2021 13:48:52'!
canNotDivideByBiggerNumberErrorDescription

	^'Can not divide by bigger number'! !


!II class methodsFor: 'as yet unclassified' stamp: 'ARM 9/2/2021 19:30:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := I.
	next := III.! !


!classDefinition: #III category: 'Natural Numbers'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'Natural Numbers'!
III class
	instanceVariableNames: ''!

!III class methodsFor: 'as yet unclassified' stamp: 'ARM 9/2/2021 19:30:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := II.
	next := IIII.! !


!classDefinition: #IIII category: 'Natural Numbers'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: 'Natural Numbers'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: 'as yet unclassified' stamp: 'ARM 9/2/2021 19:30:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := III.
	next := IIIII.! !


!classDefinition: #IIIII category: 'Natural Numbers'!
II subclass: #IIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIII class' category: 'Natural Numbers'!
IIIII class
	instanceVariableNames: ''!

!IIIII class methodsFor: 'as yet unclassified' stamp: 'ARM 9/2/2021 19:30:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := IIII.
	next := nil.! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!
IIIII initializeAfterFileIn!