!classDefinition: #I category: 'NaturalNumbers'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'NaturalNumbers'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 18:44:14'!
* aNaturalNumber

	^aNaturalNumber.! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/26/2021 21:13:55'!
+ aNaturalNumber

	^aNaturalNumber next.! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 18:36:07'!
- aNaturalNumber
	 
	^Error signal: self descripcionDeErrorDeNumerosNegativosNoSoportados.! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:31:15'!
/ aNaturalNumber
	
	aNaturalNumber = I ifFalse: [Error signal: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	
	^self.
! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:19:59'!
< aNaturalNumber 
	
	aNaturalNumber = I ifTrue: [^false].
	^true.! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:27:56'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	
	^'Ojo: el divisor no puede ser mayor al dividendo.'! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 18:17:22'!
descripcionDeErrorDeNumerosNegativosNoSoportados
	
	^'Ojo: los numeros negativos no estan implementados.'! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 18:11:02'!
leRestaA: aNaturalNumber

	^aNaturalNumber previous.! !

!I class methodsFor: 'as yet unclassified' stamp: 'Min 8/26/2021 20:50:04'!
next
	^II.! !


!classDefinition: #II category: 'NaturalNumbers'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'NaturalNumbers'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 18:43:45'!
* aNaturalNumber

	^self previous * aNaturalNumber + aNaturalNumber.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/26/2021 21:24:33'!
+ aNaturalNumber
	
	^self previous + aNaturalNumber next.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 18:10:03'!
- aNaturalNumber 
	"Pre: self > aNaturalNumber"
	
	^aNaturalNumber  leRestaA: self.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:27:04'!
/ aNaturalNumber
 	"Pre: self >= aNaturalNumber."
	self < aNaturalNumber ifTrue: [Error signal: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	
	self = aNaturalNumber ifTrue: [^I].
	
	self - aNaturalNumber < aNaturalNumber ifTrue: [^I].
	
	^self - aNaturalNumber / aNaturalNumber + I.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:19:29'!
< aNaturalNumber 

	aNaturalNumber  = I ifTrue: [^false].
	^self previous < aNaturalNumber previous.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:23:58'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	
	^'Ojo: el divisor no puede ser mayor al dividendo.'! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:40:16'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 18:13:22'!
leRestaA: aNaturalNumber
	"Pre: self < aNaturalNumber"
	
	^self previous leRestaA: aNaturalNumber previous.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/26/2021 21:26:49'!
next
	"If next had previously been created, return it; otherwise, create it."
	next ifNotNil: [^next].
	
	"Using createCloneNamed copies the code, but doesn't keep it updated; on the other hand, a child uses the parent's method, instead of copying them."
	next _ II createChildNamed: self name, 'I'.
	next previous: self.
	^next.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/26/2021 21:25:25'!
previous
	^previous.! !

!II class methodsFor: 'as yet unclassified' stamp: 'Min 8/26/2021 21:32:56'!
previous: aNaturalNumber 

	previous _ aNaturalNumber.! !


!classDefinition: #III category: 'NaturalNumbers'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'NaturalNumbers'!
III class
	instanceVariableNames: ''!

!III class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:40:16'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	previous := II.! !


!classDefinition: #IIII category: 'NaturalNumbers'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: 'NaturalNumbers'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: 'as yet unclassified' stamp: 'Min 8/29/2021 19:40:16'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := AnObsoleteIIIII.
	previous := III.! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!