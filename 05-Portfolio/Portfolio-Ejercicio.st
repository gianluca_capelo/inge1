!classDefinition: #PortfolioTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTest methodsFor: 'assertions' stamp: 'fs 10/18/2021 14:56:16'!
assert: aPortfolio canNotAdd: aPortfolioEntry

	^self
		should: [ aPortfolio add: aPortfolioEntry ]
		raise: Error 
		withExceptionDo: [ :anError |
			self assert: Portfolio addingRepeatedContentErrorDescription equals: anError messageText.].! !


!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:41:41'!
test01emptyPortfolioHasNoRegisteredTransactions
	
	|portfolio deposit withdraw |
	
	portfolio  _ Portfolio new.
	
	deposit _ Deposit for: 50.
	withdraw _ Withdraw for: 50.
		
	self deny: (portfolio hasRegistered: deposit).	
	self deny: (portfolio hasRegistered: withdraw).
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:41:59'!
test02hasRegisteredItsAccountsTransactions
	
	|portfolio deposit account1 account2|
	
	portfolio  _ Portfolio new.
	account1 _ ReceptiveAccount new.
	account2 _ ReceptiveAccount new.
	deposit _ Deposit for: 150.
	
	account2 register: deposit.
	portfolio add: account1.
	portfolio add: account2.
	
	self assert: (portfolio hasRegistered: deposit).
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:42:05'!
test03hasRegisteredItsPortfoliosTransactions
	
	|portfolio deposit anotherPortfolio account |
	
	portfolio  _ Portfolio new.
	anotherPortfolio _ Portfolio new.
	deposit _ Deposit for: 150.
	account _ ReceptiveAccount new.
	
	account register: deposit.
	anotherPortfolio add: account.
	portfolio add: anotherPortfolio.
	
	self assert: (portfolio hasRegistered: deposit).
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:42:42'!
test04emptyPortfolioHasZeroBalance
	
	|portfolio|
	
	portfolio  _ Portfolio new.
	
	self assert: 0 equals: portfolio balance.
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:45:09'!
test05canCalculateBalanceOfAddedAccounts
	
	|portfolio account anotherAccount |
	
	portfolio  _ Portfolio new.
	account _ ReceptiveAccount new.
	anotherAccount _ ReceptiveAccount new.
	
	Deposit register:150 on: anotherAccount.
	Withdraw register:32 on: anotherAccount.
	portfolio add: account.
	portfolio add: anotherAccount.
	
	self assert: 118 equals: portfolio balance.
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:46:31'!
test06canCalculateBalanceOfAddedPortfolios
	
	|portfolio2 account anotherAccount portfolio1 portfolio3 |
	
	portfolio1  _ Portfolio new.
	portfolio2  _ Portfolio new.
	portfolio3  _ Portfolio new.
	account _ ReceptiveAccount new.
	anotherAccount _ ReceptiveAccount new.
	
	Deposit register:150 on: anotherAccount.
	Withdraw register:32 on: anotherAccount.
	portfolio2 add: account.
	portfolio3 add: anotherAccount.
	portfolio1 add: portfolio2.
	portfolio1 add: portfolio3.
	
	self assert: 118 equals: portfolio1 balance.
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:47:22'!
test07emptyPortfolioHasNoTransactions
	
	|portfolio|
	
	portfolio  _ Portfolio new.
	
	self assert: portfolio transactions isEmpty.
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:48:52'!
test08canCollectTransactionsOfAddedAccounts

	|portfolio account anotherAccount deposit withdraw transactions |
	
	portfolio  _ Portfolio new.
	account _ ReceptiveAccount new.
	anotherAccount _ ReceptiveAccount new.
	deposit _ Deposit register: 150 on: anotherAccount.
	withdraw _ Withdraw register:32 on: anotherAccount.
	portfolio add: account.
	portfolio add: anotherAccount.
	
	transactions _ portfolio transactions.
	
	self assert: transactions includes: deposit.	
	self assert: transactions includes: withdraw.	
	self assert: 2 equals: transactions size. 
	
	
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:49:08'!
test09canCollectTransactionsOfAddedPortfolios
	
	|portfolio1 portfolio2 portfolio3 account account2 deposit transactions withdraw |
	
	portfolio1  _ Portfolio new.
	portfolio2  _ Portfolio new.
	portfolio3  _ Portfolio new.
	account _ ReceptiveAccount new.
	account2 _ ReceptiveAccount new.
	deposit _ Deposit register:150 on: account2.
	withdraw _ Withdraw register:32 on: account2.
	portfolio2 add: account.
	portfolio3 add: account2.
	
	portfolio1 add: portfolio2.
	portfolio1 add: portfolio3.
	
	transactions _ portfolio1 transactions.
	
	self assert: transactions includes: deposit.	
	self assert: transactions includes: withdraw.	
	self assert: 2 equals: transactions size. 
	
	
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:52:02'!
test10canNotAddAlreadyContainedAccount
	
	|portfolio1 portfolio2 account |
	
	portfolio1  _ Portfolio new.
	portfolio2  _ Portfolio new.
	account _ ReceptiveAccount new.
	portfolio2 add: account.
	portfolio1 add: portfolio2.
	
	self assert: portfolio1 canNotAdd: account.
! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:56:25'!
test10canNotAddAlreadyContainedEntry
	
	|portfolio1 portfolio2 account |
	
	portfolio1  _ Portfolio new.
	portfolio2  _ Portfolio new.
	account _ ReceptiveAccount new.
	portfolio2 add: account.
	portfolio1 add: portfolio2.
	
	self assert: portfolio1 canNotAdd: account.
	self assert: portfolio1 canNotAdd: portfolio2 .
! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:54:35'!
test11canNotAddAccountAlreadyContainedInPredecessor

	|portfolio1 portfolio2 portfolio3 account |
	
	portfolio1  _ Portfolio new.
	portfolio2  _ Portfolio new.
	portfolio3  _ Portfolio new.
	account _ ReceptiveAccount new.
	portfolio1 add: account.
	portfolio1 add: portfolio2.
	portfolio2 add: portfolio3.	
	
	self assert: portfolio3 canNotAdd: account.

! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:57:07'!
test12CanNotAddPortfolioToPortfolioThatContainsSameAccount
	
	| portfolio1 portfolio2 account |
	
	portfolio1  _ Portfolio new.
	portfolio2  _ Portfolio new.
	account _ ReceptiveAccount new.
	portfolio1 add: account.
	portfolio2 add: account.
	
	self assert: portfolio1 canNotAdd: portfolio2.


! !

!PortfolioTest methodsFor: 'tests' stamp: 'fs 10/18/2021 16:57:13'!
test13CanNotAddAccountToPortfolioWhichEitherParentContainsIt
	
	| portfolio1 portfolio2 portfolio3 account |
	
	portfolio1  _ Portfolio new.
	portfolio2  _ Portfolio new.
	portfolio3  _ Portfolio new.
	account _ ReceptiveAccount new.
	portfolio1 add: account.
	portfolio1 add: portfolio3.
	portfolio2 add: portfolio3.
	
	self assert: portfolio3 canNotAdd: account.


! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:52'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/17/2021 17:29:53'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:14:01'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'fs 10/18/2021 15:22:10'!
updatedBalance 

	self subclassResponsibility ! !

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'balance' stamp: 'fs 10/18/2021 15:21:33'!
updatedBalance: previousBalance 
	
	^previousBalance + value.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !


!Withdraw methodsFor: 'balance' stamp: 'fs 10/18/2021 16:04:09'!
updatedBalance: previousBalance 
	
	^previousBalance - value.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
Object subclass: #Portfolio
	instanceVariableNames: 'contents parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'adding' stamp: 'fs 10/18/2021 14:55:14'!
add: aPortfolioEntry

	self assertCanAdd: aPortfolioEntry.
	
	aPortfolioEntry addTo: self.! !

!Portfolio methodsFor: 'adding' stamp: 'fs 10/17/2021 22:30:38'!
addTo: aPortfolio

	parents add: aPortfolio.
	aPortfolio addWhenNotRepeated: self.! !

!Portfolio methodsFor: 'adding' stamp: 'fs 10/17/2021 22:27:47'!
addWhenNotRepeated: aPortfolioEntry
	
	contents add: aPortfolioEntry.! !


!Portfolio methodsFor: 'financial' stamp: 'fs 10/17/2021 21:07:33'!
balance
	
	^contents sum: [:aPortfolioEntry | aPortfolioEntry balance] ifEmpty: [0].

! !

!Portfolio methodsFor: 'financial' stamp: 'jpm 10/18/2021 02:09:43'!
transactions
	
	^contents 
		collect: [:aPortfolioEntry | aPortfolioEntry transactions] 
		andFold: [:anEntriesTransactions :anotherEntriesTransactions | anEntriesTransactions addAllLast: anotherEntriesTransactions] 
		ifEmpty: [OrderedCollection new.].! !


!Portfolio methodsFor: 'testing' stamp: 'jpm 10/18/2021 01:56:14'!
anyContentsIncludedIn: anotherPortfolio 
	
	^contents anySatisfy: [:aPortfolioEntry | anotherPortfolio includes: aPortfolioEntry].! !

!Portfolio methodsFor: 'testing' stamp: 'jpm 10/18/2021 02:06:45'!
canAdd: aPortfolioEntry
	
	self hasParents ifFalse: [^(self includes: aPortfolioEntry) not.].
	^parents allSatisfy: [:aParent | aParent canAdd: aPortfolioEntry.].
! !

!Portfolio methodsFor: 'testing' stamp: 'fs 10/18/2021 15:38:29'!
contentsIncludePortfolio: aPortfolio

	^contents anySatisfy: [:aPortfolioEntry | aPortfolioEntry includesPortfolio: aPortfolio.].! !

!Portfolio methodsFor: 'testing' stamp: 'fs 10/17/2021 21:54:29'!
hasParents

	^ parents notEmpty.! !

!Portfolio methodsFor: 'testing' stamp: 'fs 10/17/2021 21:38:18'!
hasRegistered: aTransaction
		
	^contents anySatisfy: [:aPortfolioEntry | aPortfolioEntry hasRegistered: aTransaction.].
	! !

!Portfolio methodsFor: 'testing' stamp: 'jpm 10/18/2021 02:01:38'!
includes: aPortfolioEntry
	
	^aPortfolioEntry isIncludedIn: self.! !

!Portfolio methodsFor: 'testing' stamp: 'jpm 10/18/2021 01:57:40'!
includesAccount: anAccount
	
	^contents anySatisfy: [:aPortfolioEntry | aPortfolioEntry includesAccount: anAccount.].! !

!Portfolio methodsFor: 'testing' stamp: 'fs 10/18/2021 15:37:43'!
includesPortfolio: aPortfolio
	
	^self = aPortfolio 
		or: [self contentsIncludePortfolio: aPortfolio.]
		or: [aPortfolio anyContentsIncludedIn: self.].! !

!Portfolio methodsFor: 'testing' stamp: 'jpm 10/18/2021 02:00:48'!
isIncludedIn: aPortfolio
	
	^aPortfolio includesPortfolio: self.! !


!Portfolio methodsFor: 'assertions' stamp: 'fs 10/18/2021 14:56:16'!
assertCanAdd: aPortfolioEntry

	(self canAdd: aPortfolioEntry) ifFalse: [self error: self class addingRepeatedContentErrorDescription ].! !


!Portfolio methodsFor: 'initialization' stamp: 'fs 10/17/2021 20:26:00'!
initialize	

	contents _ OrderedCollection new.
	parents _ OrderedCollection new.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'error descriptions' stamp: 'fs 10/18/2021 14:56:16'!
addingRepeatedContentErrorDescription
	
	^'Some of the content has already been added!!!!!!'.! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
Object subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'NR 10/17/2019 15:06:56'!
initialize

	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'gc 10/11/2021 21:13:02'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'fs 10/18/2021 16:22:14'!
balance

	^transactions inject: 0 into: [:previousBalance :aTransaction | aTransaction updatedBalance: previousBalance].! !


!ReceptiveAccount methodsFor: 'private' stamp: 'fs 10/17/2021 22:30:38'!
addTo: aPortfolio

	aPortfolio addWhenNotRepeated: self.! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'jpm 10/18/2021 01:59:54'!
includes: aPortfolioEntry
	
	^aPortfolioEntry = self! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'jpm 10/18/2021 02:03:28'!
includesAccount: anAccount
	
	^anAccount = self! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'jpm 10/18/2021 02:03:47'!
includesPortfolio: aPortfolio
	
	^false! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'jpm 10/18/2021 02:00:48'!
isIncludedIn: aPortfolio
	
	^aPortfolio includesAccount: self.! !
