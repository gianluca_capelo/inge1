!classDefinition: #MarsRoverTest category: 'MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverTest methodsFor: 'test data - directions' stamp: 'FS 10/6/2021 17:21:39'!
east

	^#East.! !

!MarsRoverTest methodsFor: 'test data - directions' stamp: 'FS 10/6/2021 17:21:44'!
north

	^#North.! !

!MarsRoverTest methodsFor: 'test data - directions' stamp: 'FS 10/6/2021 17:21:48'!
south

	^#South.! !

!MarsRoverTest methodsFor: 'test data - directions' stamp: 'FS 10/6/2021 17:21:54'!
west

	^#West.! !


!MarsRoverTest methodsFor: 'tests' stamp: 'FS 10/6/2021 18:56:20'!
test01DoesNotMoveWhenThereAreNoCommands

	| aMarsRover |
	
	aMarsRover _ MarsRover at: 0@0 heading: self north.
	
	aMarsRover process: ''.
	
	self assert: (aMarsRover isAt: 0@0 heading: self north).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FS 10/6/2021 19:29:41'!
test02CanStepForward

	| aMarsRover |
	
	aMarsRover _ MarsRover at: 0@0 heading: self east.
	
	aMarsRover process: 'f'.
	
	self assert: (aMarsRover isAt: 1@0 heading: self east).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FS 10/6/2021 19:29:51'!
test03CanStepBackward

	| aMarsRover |
	
	aMarsRover _ MarsRover at: 0@0 heading: self south.
	
	aMarsRover process: 'b'.
	
	self assert: (aMarsRover isAt: 0@1 heading: self south).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FS 10/6/2021 19:30:54'!
test04CanTurnLeft

	| aMarsRover |
	
	aMarsRover _ MarsRover at: 0@0 heading: self north.
	
	aMarsRover process: 'l'.
	
	self assert: (aMarsRover isAt: 0@0 heading: self west).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FS 10/6/2021 19:31:02'!
test05CanTurnRight

	| aMarsRover |
	
	aMarsRover _ MarsRover at: 0@0 heading: self west.
	
	aMarsRover process: 'r'.
	
	self assert: (aMarsRover isAt: 0@0 heading: self north).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FS 10/6/2021 19:29:09'!
test06CanProcessMultipleCommands

	| aMarsRover |
	
	aMarsRover _ MarsRover at: 0@0 heading: self south.
	
	aMarsRover process: 'fl'.
	
	self assert: (aMarsRover isAt: 0@-1 heading: self east).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FS 10/6/2021 19:29:16'!
test07StopsProcessingAfterInvalidCommand

	| aMarsRover |
	
	aMarsRover _ MarsRover at: 0@0 heading: self south.
	
	aMarsRover process: 'fxf'.
	
	self assert: (aMarsRover isAt: 0@-1 heading: self south).! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'position direction point cardinalPoint'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'initialization' stamp: 'FS 10/6/2021 18:11:54'!
initializeAt: aPoint heading: aDirection 

	position _ aPoint.
	direction _ MarsRoverDirection for: self heading: aDirection.	! !


!MarsRover methodsFor: 'operating' stamp: 'FS 10/5/2021 16:07:47'!
process: aSequenceOfCommands 

	aSequenceOfCommands do: [:aCommand | 
		(self isAValidCommand: aCommand)
			ifTrue: [self processCommand: aCommand.]
			ifFalse: [^self.].
		].! !


!MarsRover methodsFor: 'testing' stamp: 'FS 10/5/2021 16:40:55'!
isAt: aPoint

	^ position = aPoint! !

!MarsRover methodsFor: 'testing' stamp: 'FS 10/6/2021 16:26:41'!
isAt: aPoint heading: aDirection 
	
	^ (self isAt: aPoint) and: [self isHeading: aDirection.].! !

!MarsRover methodsFor: 'testing' stamp: 'FS 10/6/2021 17:51:20'!
isHeading: aDirection

	^ direction isHeading: aDirection.! !


!MarsRover methodsFor: 'private' stamp: 'FS 10/5/2021 16:10:46'!
isAValidCommand: anUnvalidatedCommand
	
	^ anUnvalidatedCommand = $f 
		or: [anUnvalidatedCommand = $b]
		or: [anUnvalidatedCommand = $l]
		or: [anUnvalidatedCommand = $r].! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/6/2021 18:38:46'!
processCommand: anUnidentifiedCommand

	anUnidentifiedCommand = $f ifTrue: [^self stepForward.	].		
	
	anUnidentifiedCommand = $b ifTrue: [^self stepBackward.].
	
	anUnidentifiedCommand = $l ifTrue: [^self turnLeft.		].
	
	anUnidentifiedCommand = $r ifTrue: [^self turnRight.		].
! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/6/2021 18:27:47'!
stepBackward

	^direction stepBackward.! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/6/2021 16:42:06'!
stepEast

	^position _ position + (1@0).! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/6/2021 16:47:16'!
stepForward

	^direction stepForward.! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/6/2021 16:41:54'!
stepNorth

	^position _ position + (0@1).! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/6/2021 16:42:16'!
stepSouth

	^position _ position + (0@-1).! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/6/2021 16:42:24'!
stepWest

	^position _ position + (-1@0).! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/5/2021 16:29:09'!
turnLeft
	
	^direction _ direction left.! !

!MarsRover methodsFor: 'private' stamp: 'FS 10/5/2021 16:29:31'!
turnRight
	
	direction _ direction right.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'instance creation' stamp: 'FS 10/6/2021 19:35:58'!
at: aPoint heading: aDirection 

	self assertValidDirection: aDirection.

	^self new initializeAt: aPoint heading: aDirection. ! !


!MarsRover class methodsFor: 'error descriptions' stamp: 'FS 10/6/2021 19:41:11'!
invalidDirectionErrorDescription

	^'Invalid direction!!!!!! Direction has to be North, East, West or South.'.! !


!MarsRover class methodsFor: 'assertions' stamp: 'FS 10/6/2021 19:40:08'!
assertValidDirection: anUnvalidatedDirection

	(anUnvalidatedDirection = #North or:
	[anUnvalidatedDirection = #East] or:
	[anUnvalidatedDirection = #South] or:
	[anUnvalidatedDirection = #West]) 
			ifFalse: [^self error: self invalidDirectionErrorDescription.].! !


!classDefinition: #MarsRoverDirection category: 'MarsRover'!
Object subclass: #MarsRoverDirection
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverDirection methodsFor: 'initialization' stamp: 'FS 10/6/2021 16:50:01'!
initializeFor: aMarsRover

	marsRover _ aMarsRover! !


!MarsRoverDirection methodsFor: 'testing' stamp: 'FS 10/6/2021 17:18:15'!
isHeading: aDirection

	^self subclassResponsibility.! !


!MarsRoverDirection methodsFor: 'directions' stamp: 'FS 10/5/2021 16:21:40'!
left

	self subclassResponsibility.! !

!MarsRoverDirection methodsFor: 'directions' stamp: 'FS 10/5/2021 16:21:43'!
right

	self subclassResponsibility.! !

!MarsRoverDirection methodsFor: 'directions' stamp: 'FS 10/6/2021 18:28:02'!
stepBackward

	self subclassResponsibility.! !

!MarsRoverDirection methodsFor: 'directions' stamp: 'FS 10/6/2021 16:45:51'!
stepForward

	self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverDirection class' category: 'MarsRover'!
MarsRoverDirection class
	instanceVariableNames: ''!

!MarsRoverDirection class methodsFor: 'instance creation' stamp: 'FS 10/6/2021 16:49:44'!
for: aMarsRover

	^self new initializeFor: aMarsRover.! !

!MarsRoverDirection class methodsFor: 'instance creation' stamp: 'FS 10/6/2021 18:18:03'!
for: aMarsRover heading: aDirection

	^(self subclasses detect: [:aSubclass | aSubclass isHeading: aDirection.]) for: aMarsRover.! !

!MarsRoverDirection class methodsFor: 'instance creation' stamp: 'FS 10/6/2021 21:23:05'!
isHeading: aDirection

	^self subclassResponsibility.! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover'!
MarsRoverDirection subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingEast methodsFor: 'directions' stamp: 'FS 10/6/2021 21:24:11'!
left

	^ MarsRoverHeadingNorth for: marsRover.! !

!MarsRoverHeadingEast methodsFor: 'directions' stamp: 'FS 10/6/2021 21:24:26'!
right

	^ MarsRoverHeadingSouth for: marsRover.! !

!MarsRoverHeadingEast methodsFor: 'directions' stamp: 'FS 10/6/2021 18:28:20'!
stepBackward

	marsRover stepWest.! !

!MarsRoverHeadingEast methodsFor: 'directions' stamp: 'FS 10/6/2021 16:46:07'!
stepForward

	marsRover stepEast.! !


!MarsRoverHeadingEast methodsFor: 'testing' stamp: 'FS 10/6/2021 18:26:29'!
isHeading: aDirection

	^aDirection = #East. 
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingEast class' category: 'MarsRover'!
MarsRoverHeadingEast class
	instanceVariableNames: ''!

!MarsRoverHeadingEast class methodsFor: 'as yet unclassified' stamp: 'FS 10/6/2021 18:22:18'!
isHeading: aDirection

	^aDirection = #East. ! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover'!
MarsRoverDirection subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingNorth methodsFor: 'testing' stamp: 'FS 10/6/2021 18:22:34'!
isHeading: aDirection

	^aDirection = #North.! !


!MarsRoverHeadingNorth methodsFor: 'directions' stamp: 'FS 10/6/2021 21:24:36'!
left

	^ MarsRoverHeadingWest for: marsRover.! !

!MarsRoverHeadingNorth methodsFor: 'directions' stamp: 'FS 10/6/2021 21:23:58'!
right

	^ MarsRoverHeadingEast for: marsRover.! !

!MarsRoverHeadingNorth methodsFor: 'directions' stamp: 'FS 10/6/2021 18:28:31'!
stepBackward

	marsRover stepSouth.! !

!MarsRoverHeadingNorth methodsFor: 'directions' stamp: 'FS 10/6/2021 16:46:22'!
stepForward

	marsRover stepNorth .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingNorth class' category: 'MarsRover'!
MarsRoverHeadingNorth class
	instanceVariableNames: ''!

!MarsRoverHeadingNorth class methodsFor: 'as yet unclassified' stamp: 'FS 10/6/2021 18:22:32'!
isHeading: aDirection

	^aDirection = #North.! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover'!
MarsRoverDirection subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingSouth methodsFor: 'testing' stamp: 'FS 10/6/2021 18:22:42'!
isHeading: aDirection

	^aDirection = #South.! !


!MarsRoverHeadingSouth methodsFor: 'directions' stamp: 'FS 10/6/2021 21:23:58'!
left

	^ MarsRoverHeadingEast for: marsRover.! !

!MarsRoverHeadingSouth methodsFor: 'directions' stamp: 'FS 10/6/2021 21:24:36'!
right

	^ MarsRoverHeadingWest for: marsRover.! !

!MarsRoverHeadingSouth methodsFor: 'directions' stamp: 'FS 10/6/2021 18:28:40'!
stepBackward

	marsRover stepNorth.! !

!MarsRoverHeadingSouth methodsFor: 'directions' stamp: 'FS 10/6/2021 16:46:31'!
stepForward

	marsRover stepSouth.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingSouth class' category: 'MarsRover'!
MarsRoverHeadingSouth class
	instanceVariableNames: ''!

!MarsRoverHeadingSouth class methodsFor: 'as yet unclassified' stamp: 'FS 10/6/2021 18:22:40'!
isHeading: aDirection

	^aDirection = #South.! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover'!
MarsRoverDirection subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingWest methodsFor: 'testing' stamp: 'FS 10/6/2021 18:22:56'!
isHeading: aDirection

	^aDirection = #West.! !


!MarsRoverHeadingWest methodsFor: 'directions' stamp: 'FS 10/6/2021 21:24:26'!
left

	^ MarsRoverHeadingSouth for: marsRover.! !

!MarsRoverHeadingWest methodsFor: 'directions' stamp: 'FS 10/6/2021 21:24:11'!
right

	^ MarsRoverHeadingNorth for: marsRover.! !

!MarsRoverHeadingWest methodsFor: 'directions' stamp: 'FS 10/6/2021 18:29:01'!
stepBackward

	marsRover stepEast.! !

!MarsRoverHeadingWest methodsFor: 'directions' stamp: 'FS 10/6/2021 16:46:39'!
stepForward

	marsRover stepWest.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingWest class' category: 'MarsRover'!
MarsRoverHeadingWest class
	instanceVariableNames: ''!

!MarsRoverHeadingWest class methodsFor: 'as yet unclassified' stamp: 'FS 10/6/2021 18:22:53'!
isHeading: aDirection

	^aDirection = #West.! !
