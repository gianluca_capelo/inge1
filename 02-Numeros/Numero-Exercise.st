!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'Min 9/14/2021 11:55:03'!
- aSubtrahend

	^self subclassResponsibility.! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !


!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:28:34'!
addAFraccion: anAdderFraccion

	^ self subclassResponsibility .! !

!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:26:05'!
addAnEntero: aAdderEntero

	^ self subclassResponsibility .! !

!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:28:15'!
divideAFraccion: aDividendFraccion

	^ self subclassResponsibility .! !

!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:27:07'!
divideAnEntero: aDividendEntero

	^ self subclassResponsibility .! !

!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:28:01'!
multiplyAFraccion: aMultiplierFraccion

	^ self subclassResponsibility .! !

!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:26:53'!
multiplyAnEntero: aMultiplierEntero

	^ self subclassResponsibility .! !

!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:27:42'!
subtractFromAFraccion: aMinuendFraccion

	^ self subclassResponsibility .! !

!Numero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:27:23'!
subtractFromAnEntero: aMinuendEntero

	^ self subclassResponsibility .! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:09'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de n�mero inv�lido!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'Min 9/13/2021 20:39:00'!
* aMultiplier 
	^aMultiplier multiplyAnEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'Min 9/13/2021 20:37:45'!
+ anAdder
	^anAdder addAnEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'Min 9/14/2021 12:02:31'!
- aSubtrahend

	^aSubtrahend subtractFromAnEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'Min 9/15/2021 16:24:01'!
/ aDivisor 
			
	^ self subclassResponsibility .! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:55'!
// aDivisor 
	
	^self class with: value // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'Min 9/13/2021 21:46:48'!
fibonacci
	
	^self subclassResponsibility .
	
	
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:00'!
greatestCommonDivisorWith: anEntero 
	
	^self class with: (value gcd: anEntero integerValue)! !


!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 21:01'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'Min 9/15/2021 17:37:46'!
initializeWith: aValue 
	
	value := aValue.! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'testing' stamp: 'NR 9/23/2018 22:17:55'!
isNegative
	
	^value < 0! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:14'!
isOne
	
	^value = 1! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:12'!
isZero
	
	^value = 0! !


!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/13/2021 20:37:18'!
addAFraccion: anAdderFraccion

	^Fraccion with: anAdderFraccion numerator + (anAdderFraccion denominator * self) over: anAdderFraccion denominator! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/13/2021 20:37:45'!
addAnEntero: anAdderEntero

	^ self class with: value + anAdderEntero integerValue! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:31:36'!
divideACero

	^ self subclassResponsibility .! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 15:45:22'!
divideAFraccion: aDividendFraccion

	^ Fraccion with: aDividendFraccion numerator over: (self * aDividendFraccion denominator) .! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:10:46'!
divideAnEnteroMayorAUno: EnteroMayorAUno

	^ self subclassResponsibility .! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:01:26'!
divideAnEnteroMenorACero: aDividendEnteroMenorACero

	^ self subclassResponsibility .! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:48:44'!
divideAnUno

	^ self subclassResponsibility .! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/13/2021 20:38:41'!
multiplyAFraccion: aMultiplierFraccion

	^ Fraccion with: aMultiplierFraccion numerator * self over: aMultiplierFraccion denominator! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/13/2021 20:39:00'!
multiplyAnEntero: aMultiplierEntero

	^ self class with: value * aMultiplierEntero integerValue! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:23:10'!
subtractFromAFraccion: aMinuendFraccion 
	
	| newNumerator |
	
	newNumerator := aMinuendFraccion numerator - (aMinuendFraccion denominator * self).
	
	^Fraccion with: newNumerator over: aMinuendFraccion denominator.! !

!Entero methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:01:07'!
subtractFromAnEntero: aSubtrahendEntero

	^self class with: aSubtrahendEntero integerValue - value.
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'Min 9/14/2021 21:23:07'!
correspondsToInteger: anIntegerValue

	self subclassResponsibility.
! !

!Entero class methodsFor: 'instance creation' stamp: 'Min 9/15/2021 17:37:46'!
with: aValue 
	
	self assertInitValueIsInteger: aValue.
		
	^(self subclasses detect: [:anEnteroSubclass | anEnteroSubclass correspondsToInteger: aValue]) new initializeWith: aValue.! !


!Entero class methodsFor: 'error descriptions' stamp: 'Min 9/13/2021 15:48:19'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no est� definido aqu� para Enteros Negativos!!!!!!'! !


!Entero class methodsFor: 'assertions' stamp: 'Min 9/13/2021 16:34:51'!
assertInitValueIsInteger: aValue

	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	^self assert: aValue isInteger description: 'aValue debe ser anInteger'.! !


!classDefinition: #Cero category: 'Numero-Exercise'!
Entero subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!
!Cero commentStamp: '<historical>' prior: 0!
Cero new initializeWith: 0.!


!Cero methodsFor: 'arithmetic operations' stamp: 'Min 9/15/2021 16:27:20'!
/ aDivisor 
			
	^aDivisor divideACero .! !

!Cero methodsFor: 'arithmetic operations' stamp: 'Min 9/15/2021 16:57:46'!
fibonacci
		
	^ Uno withOne .! !


!Cero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:53:51'!
divideACero

	^ self error: self class canNotDivideByZeroErrorDescription .! !

!Cero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 15:47:00'!
divideAnEntero: aDividendEntero

	^ self error: self class canNotDivideByZeroErrorDescription .! !

!Cero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:53:48'!
divideAnEnteroMayorAUno: EnteroMayorAUno

	^ self error: self class canNotDivideByZeroErrorDescription .! !

!Cero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:53:42'!
divideAnEnteroMenorACero: aDividendEnteroMenorACero

	^ self error: self class canNotDivideByZeroErrorDescription .! !

!Cero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 18:05:34'!
divideAnEnteroWithoutSimplification: aDividendEntero

	^ self error: self class canNotDivideByZeroErrorDescription .! !

!Cero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:53:20'!
divideAnUno

	^ self error: self class canNotDivideByZeroErrorDescription .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cero class' category: 'Numero-Exercise'!
Cero class
	instanceVariableNames: ''!

!Cero class methodsFor: 'instance creation' stamp: 'Min 9/15/2021 16:43:41'!
a

	^ Cero new initializeWith: 0 .! !

!Cero class methodsFor: 'instance creation' stamp: 'Min 9/14/2021 21:24:39'!
with: aValue

	^self superclass with: aValue.! !

!Cero class methodsFor: 'instance creation' stamp: 'Min 9/15/2021 16:57:21'!
withZero

	^ self new initializeWith: 0 .! !


!Cero class methodsFor: 'testing' stamp: 'Min 9/14/2021 21:25:14'!
correspondsToInteger: anIntegerValue

	^anIntegerValue = 0.! !


!classDefinition: #EnteroMayorAUno category: 'Numero-Exercise'!
Entero subclass: #EnteroMayorAUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroMayorAUno methodsFor: 'arithmetic operations' stamp: 'Min 9/15/2021 17:08:10'!
/ aDividend
	
	^ aDividend divideAnEnteroMayorAUno: self .! !

!EnteroMayorAUno methodsFor: 'arithmetic operations' stamp: 'Min 9/15/2021 16:58:03'!
fibonacci
	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.
	
	^ (self - one) fibonacci + (self - two) fibonacci.! !


!EnteroMayorAUno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:44:58'!
divideACero

	^ Cero withZero .! !

!EnteroMayorAUno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:23:00'!
divideAnEntero: aDividendEntero

	! !

!EnteroMayorAUno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:27:12'!
divideAnEnteroMayorAUno: aDividendEnteroMayorAUno

	| greatestCommonDivisor simplifiedDividend simplifiedDivisor |

	greatestCommonDivisor := aDividendEnteroMayorAUno greatestCommonDivisorWith: self. 
	simplifiedDividend := aDividendEnteroMayorAUno // greatestCommonDivisor.
	simplifiedDivisor := self // greatestCommonDivisor.
		
	^ simplifiedDivisor divideAnEnteroWithoutSimplification: simplifiedDividend .! !

!EnteroMayorAUno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:27:58'!
divideAnEnteroMenorACero: aDividendEnteroMenorACero

	| greatestCommonDivisor simplifiedDividend simplifiedDivisor |

	greatestCommonDivisor := aDividendEnteroMenorACero greatestCommonDivisorWith: self. 
	simplifiedDividend := aDividendEnteroMenorACero // greatestCommonDivisor.
	simplifiedDivisor := self // greatestCommonDivisor.
		
	^ simplifiedDivisor divideAnEnteroWithoutSimplification: simplifiedDividend .! !

!EnteroMayorAUno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:26:34'!
divideAnEnteroWithoutSimplification: aDividendEntero

	^ Fraccion new initializeWith: aDividendEntero over: self .! !

!EnteroMayorAUno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:56:16'!
divideAnUno

	^ Fraccion new initializeWith: Uno withOne over: self .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroMayorAUno class' category: 'Numero-Exercise'!
EnteroMayorAUno class
	instanceVariableNames: ''!

!EnteroMayorAUno class methodsFor: 'instance creation' stamp: 'Min 9/13/2021 21:40:31'!
with: aValue

	^self superclass with: aValue.! !


!EnteroMayorAUno class methodsFor: 'testing' stamp: 'Min 9/14/2021 21:23:07'!
correspondsToInteger: anIntegerValue

	^anIntegerValue > 1.! !


!classDefinition: #EnteroMenorACero category: 'Numero-Exercise'!
Entero subclass: #EnteroMenorACero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroMenorACero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:45:21'!
divideACero

	^ Cero withZero .! !

!EnteroMenorACero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 15:51:11'!
divideAnEntero: aDividendEntero

	^ aDividendEntero negated / self negated .! !

!EnteroMenorACero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:11:38'!
divideAnEnteroMayorAUno: aDividendEnteroMayorAUno

	^ aDividendEnteroMayorAUno negated / self negated .! !

!EnteroMenorACero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:03:01'!
divideAnEnteroMenorACero: aDividendEnteroMenorACero

	^ aDividendEnteroMenorACero negated / self negated .! !

!EnteroMenorACero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 18:06:50'!
divideAnEnteroWithoutSimplification: aDividendEntero

	^ self negated divideAnEnteroWithoutSimplification: aDividendEntero negated.
! !

!EnteroMenorACero methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:51:24'!
divideAnUno

	^ Fraccion new initializeWith: (self class with: -1) over: self negated .! !


!EnteroMenorACero methodsFor: 'arithmetic operations' stamp: 'Min 9/15/2021 17:00:56'!
/ aDivisor

	^ aDivisor divideAnEnteroMenorACero: self .! !

!EnteroMenorACero methodsFor: 'arithmetic operations' stamp: 'Min 9/13/2021 21:41:48'!
fibonacci
	
	^self error: self class negativeFibonacciErrorDescription.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroMenorACero class' category: 'Numero-Exercise'!
EnteroMenorACero class
	instanceVariableNames: ''!

!EnteroMenorACero class methodsFor: 'instance creation' stamp: 'Min 9/13/2021 21:38:28'!
with: aValue

	^self superclass with: aValue.! !


!EnteroMenorACero class methodsFor: 'testing' stamp: 'Min 9/14/2021 21:23:07'!
correspondsToInteger: anIntegerValue

	^anIntegerValue < 0.! !


!classDefinition: #Uno category: 'Numero-Exercise'!
Entero subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:45:30'!
divideACero

	^ Cero withZero .! !

!Uno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:10:19'!
divideAnEnteroMayorAUno: aDividendEnteroMayorAUno

	^ aDividendEnteroMayorAUno .! !

!Uno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:02:06'!
divideAnEnteroMenorACero: aDividendEnteroMenorACero

	^ aDividendEnteroMenorACero .! !

!Uno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:26:58'!
divideAnEnteroWithoutSimplification: aDividendEntero

	^ aDividendEntero .! !

!Uno methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 16:47:58'!
divideAnUno

	^ self .! !


!Uno methodsFor: 'arithmetic operations' stamp: 'Min 9/15/2021 16:47:31'!
/ aDivisor 

	^ aDivisor divideAnUno .! !

!Uno methodsFor: 'arithmetic operations' stamp: 'Min 9/14/2021 21:31:51'!
fibonacci
	
	^ self .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Uno class' category: 'Numero-Exercise'!
Uno class
	instanceVariableNames: ''!

!Uno class methodsFor: 'instance creation' stamp: 'Min 9/14/2021 21:25:32'!
with: aValue

	^self superclass with: aValue.! !

!Uno class methodsFor: 'instance creation' stamp: 'Min 9/15/2021 17:37:17'!
withOne

	^ self new initializeWith: 1.! !


!Uno class methodsFor: 'testing' stamp: 'Min 9/14/2021 21:25:38'!
correspondsToInteger: anIntegerValue

	^anIntegerValue = 1.! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'Min 9/13/2021 20:38:41'!
* aMultiplier 
	^aMultiplier multiplyAFraccion: self
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'Min 9/13/2021 20:37:18'!
+ anAdder 
	
	^anAdder addAFraccion: self.
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'Min 9/14/2021 12:03:00'!
- aSubtrahend

	^aSubtrahend subtractFromAFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'Min 9/13/2021 20:38:10'!
/ aDivisor 
	
	^aDivisor divideAFraccion: self.! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !


!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/13/2021 20:37:18'!
addAFraccion: anAdderFraccion

	| newNumerator newDenominator |
		
	newNumerator := (numerator * anAdderFraccion denominator) + (denominator * anAdderFraccion numerator).
	newDenominator := denominator * anAdderFraccion denominator.
	
	^self class with: newNumerator over: newDenominator! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:20:38'!
addAnEntero: anAdderEntero

	^anAdderEntero addAFraccion: self.! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:56:56'!
divideACero

	^ Cero withZero .! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:20:42'!
divideAFraccion: aDividendFraccion

	^self class with: aDividendFraccion numerator * denominator over: (aDividendFraccion denominator * numerator).! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:59:47'!
divideAnEnteroMayorAUno: aDividendEnteroMayorAUno

	^self class with: self denominator * aDividendEnteroMayorAUno over: self numerator ! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 18:00:09'!
divideAnEnteroMenorACero: aDividendEnteroMenorACero

	^self class with: self denominator * aDividendEnteroMenorACero over: self numerator ! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/15/2021 17:58:19'!
divideAnUno

	^  Fraccion with: denominator over: numerator .! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:20:52'!
multiplyAFraccion: aMultiplierFraccion

	^self class with: numerator * aMultiplierFraccion numerator over: (denominator * aMultiplierFraccion denominator).! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/13/2021 20:39:00'!
multiplyAnEntero: aMultiplierEntero

	^ aMultiplierEntero multiplyAFraccion: self! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:13:43'!
subtractFromAFraccion: aMinuendFraccion 
	
	| newNumerator newDenominator |
	
	newNumerator := (aMinuendFraccion numerator * denominator) - (aMinuendFraccion denominator * numerator).
	newDenominator := aMinuendFraccion denominator * denominator.
	
	^self class with: newNumerator over: newDenominator.! !

!Fraccion methodsFor: 'auxiliar operations' stamp: 'Min 9/14/2021 12:11:53'!
subtractFromAnEntero: aMinuendEntero 
	
	| newNumerator |
	
	newNumerator := (aMinuendEntero * denominator) - numerator.
	
	^self class with: newNumerator over: denominator.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'Min 9/15/2021 15:39:34'!
with: aDividend over: aDivisor

	^ aDividend / aDivisor.
	! !
