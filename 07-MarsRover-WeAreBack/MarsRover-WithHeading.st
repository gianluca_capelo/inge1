!classDefinition: #MarsRoverChangeReceptorTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverChangeReceptorTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:26:21'!
test01EmptyLogWhenNoPositionChanges

	| logger aMarsRover expectedLogs |
	
	aMarsRover := MarsRover at: 1@2 heading: self east. 
	
	logger := self logPositionChangesOf: aMarsRover.
		
	expectedLogs := {}.
	
	self assert: logger hasLogged: expectedLogs.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:25:52'!
test02CanLogPositionChanges

	| logger aMarsRover expectedLogs |
	
	aMarsRover := MarsRover at: 0@0 heading: self north. 

	
	logger := self logPositionChangesOf: aMarsRover.
	
	aMarsRover process: 'frfrfrflblblblb'.
	
	expectedLogs := {'0@1'. '1@1'. '1@0'. '0@0'. '0@1'. '-1@1'. '-1@0'. '0@0'.}.
	
	self assert: logger hasLogged: expectedLogs.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:29:49'!
test03EmptyLogWhenNoHeadChanges

	| logger aMarsRover expectedLogs |
	
	aMarsRover := MarsRover at: 1@2 heading: self east. 
	
	logger := self logHeadingChangesOf: aMarsRover.
	
	expectedLogs := {}.
	self assert: logger hasLogged: expectedLogs.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:31:35'!
test04CanLogHeadChanges

	| logger aMarsRover expectedLogs |
	
	aMarsRover := MarsRover at: 1@2 heading: self north. 
	
	
	logger := self logHeadingChangesOf: aMarsRover.
	
	aMarsRover process: 'rfrfrfrflblblblb'.
	
	expectedLogs := { 'East'. 'South'. 'West'. 'North'. 'West'. 'South'. 'East'. 'North'. }.
	
	self assert: logger hasLogged: expectedLogs.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:35:11'!
test05CanLogPositionAndHeadChanges

	| aMarsRover logger expectedLogs |
	
	aMarsRover := MarsRover at: 0@0 heading: self north. 
	
	logger := self logPositionAndHeadingChangesOf: aMarsRover.
	 
	aMarsRover process: 'frfr'.
	
	expectedLogs := {'0@1'. 'East'. '1@1'. 'South'}.
	
	self assert: logger hasLogged: expectedLogs.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:37:41'!
test06DoesNotDisplayWhenNoPositionChanges

	| aMarsRover tracker |
	
	aMarsRover := MarsRover at: 0@0 heading: self north. 
	
	tracker := self currentPositionWindowFor: aMarsRover.
	
	self assert: tracker isAt: 'N/A'.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:45:41'!
test07CanDisplayCurrentPosition

	| aMarsRover window |
	
	aMarsRover := MarsRover at: 0@0 heading: self north. 
	
	window := self currentPositionWindowFor: aMarsRover.
	
	aMarsRover process: 'frflflb'.
	
	self assert: window isAt: '2@2'.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:45:48'!
test08CanDisplayCurrentHead

	| aMarsRover window |
	
	aMarsRover := MarsRover at: 0@0 heading: self north. 
	
	window := self currentHeadWindowFor: aMarsRover.
	
	aMarsRover process: 'frflflb'.
	
	self assert: window isHeading: 'West'.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:47:09'!
test09CanDisplayCurrentPositionAndHead

	| aMarsRover window |
	
	aMarsRover := MarsRover at: 0@0 heading: self north. 
	
	window := self currentHeadAndPositionWindowFor: aMarsRover.
	
	aMarsRover process: 'frfr'.
	
	self assert: window isAt: '1@1'.
	self assert: window isHeading: 'South'.! !

!MarsRoverChangeReceptorTest methodsFor: 'testing' stamp: 'AT 10/28/2021 16:54:26'!
test10MultipleLogsAndWindows

	| aMarsRover positionLogger headLogger composedLogger positionWindow headWindow composedWindow |
	
	aMarsRover := MarsRover at: 0@0 heading: self north. 
	
	positionLogger _ self logPositionChangesOf: aMarsRover.
	headLogger _ self logHeadingChangesOf: aMarsRover.
	composedLogger _ self logPositionAndHeadingChangesOf: aMarsRover.
	
	positionWindow _ self currentPositionWindowFor: aMarsRover.
	headWindow _ self currentHeadWindowFor: aMarsRover.
	composedWindow _ self currentHeadAndPositionWindowFor: aMarsRover.
	
	aMarsRover process: 'fr'.
	
	self assert: positionLogger hasLogged: { '0@1'. }.
	self assert: headLogger hasLogged: { 'East'. }.
	self assert: composedLogger hasLogged: { '0@1'. 'East'. }.
	
	self assert: positionWindow isAt: '0@1'.
	self assert: headWindow isHeading: 'East'.
	
	self assert: composedWindow isAt: '0@1' heading: 'East'.	! !


!MarsRoverChangeReceptorTest methodsFor: 'directions' stamp: 'jpm 10/26/2021 21:28:41'!
east

	^ MarsRoverHeadingEast! !

!MarsRoverChangeReceptorTest methodsFor: 'directions' stamp: 'jpm 10/26/2021 21:29:04'!
north

	^ MarsRoverHeadingNorth! !

!MarsRoverChangeReceptorTest methodsFor: 'directions' stamp: 'jpm 10/26/2021 21:29:23'!
south

	^ MarsRoverHeadingSouth! !

!MarsRoverChangeReceptorTest methodsFor: 'directions' stamp: 'jpm 10/26/2021 21:29:35'!
west

	^ MarsRoverHeadingWest! !


!MarsRoverChangeReceptorTest methodsFor: 'logging' stamp: 'AT 10/27/2021 21:32:49'!
logHeadingChangesOf: aMarsRover
		
	^ HeadLogger on: Logger new for: aMarsRover.		
! !

!MarsRoverChangeReceptorTest methodsFor: 'logging' stamp: 'AT 10/28/2021 13:43:23'!
logPositionAndHeadingChangesOf: aMarsRover 
	
	^ PositionLogger on: (HeadLogger on: Logger new for: aMarsRover) for: aMarsRover.! !

!MarsRoverChangeReceptorTest methodsFor: 'logging' stamp: 'AT 10/27/2021 21:22:35'!
logPositionChangesOf: aMarsRover
	
	^ PositionLogger on: Logger new for: aMarsRover.! !


!MarsRoverChangeReceptorTest methodsFor: 'window' stamp: 'AT 10/28/2021 16:47:09'!
currentHeadAndPositionWindowFor: aMarsRover 
	
	^ WindowPositionReceptor on: (WindowHeadReceptor on: Window new for: aMarsRover) for: aMarsRover.! !

!MarsRoverChangeReceptorTest methodsFor: 'window' stamp: 'AT 10/28/2021 16:45:27'!
currentHeadWindowFor: aMarsRover 
	
	^ WindowHeadReceptor on: Window new for: aMarsRover.! !

!MarsRoverChangeReceptorTest methodsFor: 'window' stamp: 'AT 10/28/2021 16:37:18'!
currentPositionWindowFor: aMarsRover 
	
	^ WindowPositionReceptor on: Window new for: aMarsRover.! !


!MarsRoverChangeReceptorTest methodsFor: 'assertions' stamp: 'AT 10/28/2021 14:02:08'!
assert: aLogger hasLogged: aCollectionOfLogs
	
	| reader |
	
	reader := aLogger logReader.
	
	aCollectionOfLogs do: [ :log |
		self assert: log equals: reader nextLine.
		].
	
	self assert: reader atEnd.! !

!MarsRoverChangeReceptorTest methodsFor: 'assertions' stamp: 'AT 10/28/2021 13:31:18'!
assert: aPositionWindow isAt: aPosition
	
	self assert: aPosition equals: (aPositionWindow atField: #Position).! !

!MarsRoverChangeReceptorTest methodsFor: 'assertions' stamp: 'AT 10/28/2021 16:55:13'!
assert: aComposedWindow isAt: aPosition heading: aHead
	
	self assert: aComposedWindow isAt: aPosition.
	self assert: aComposedWindow isHeading: aHead.
! !

!MarsRoverChangeReceptorTest methodsFor: 'assertions' stamp: 'AT 10/28/2021 13:32:09'!
assert: aHeadWindow isHeading: aDirection
	
	self assert: aDirection equals: (aHeadWindow atField: #Head).! !


!classDefinition: #MarsRoverTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #ChangeBroadcaster category: 'MarsRover-WithHeading'!
Object subclass: #ChangeBroadcaster
	instanceVariableNames: 'positionChangeReceptors headChangeReceptors'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!ChangeBroadcaster methodsFor: 'initialization' stamp: 'AT 10/28/2021 16:03:24'!
initialize

	positionChangeReceptors _ OrderedCollection new.
	headChangeReceptors _ OrderedCollection new.! !


!ChangeBroadcaster methodsFor: 'broadcasting' stamp: 'AT 10/28/2021 16:08:04'!
broadcast: aNewChange to: aReceptorGroup 
	
	aReceptorGroup do: [ :aReceptor | aReceptor updateChange: aNewChange. ].! !

!ChangeBroadcaster methodsFor: 'broadcasting' stamp: 'AT 10/28/2021 16:05:26'!
notifyHeadChangesTo: aNewReceptor 
	
	headChangeReceptors add: aNewReceptor.! !

!ChangeBroadcaster methodsFor: 'broadcasting' stamp: 'AT 10/28/2021 16:04:59'!
notifyPositionChangesTo: aNewReceptor 
	
	positionChangeReceptors add: aNewReceptor.! !

!ChangeBroadcaster methodsFor: 'broadcasting' stamp: 'AT 10/28/2021 16:07:16'!
updateHead: aNewHead 
	
	self broadcast: aNewHead to: headChangeReceptors.! !

!ChangeBroadcaster methodsFor: 'broadcasting' stamp: 'AT 10/28/2021 16:08:32'!
updatePosition: aNewPosition 
	
	self broadcast: aNewPosition to: positionChangeReceptors.! !


!classDefinition: #Logger category: 'MarsRover-WithHeading'!
Object subclass: #Logger
	instanceVariableNames: 'log'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Logger methodsFor: 'initialization' stamp: 'AT 10/27/2021 21:28:24'!
initialize

	log _ WriteStream on: ''.! !


!Logger methodsFor: 'accessing' stamp: 'AT 10/28/2021 14:01:25'!
logReader
	
	 ^ ReadStream on: log contents.! !


!Logger methodsFor: 'logging' stamp: 'AT 10/27/2021 21:37:56'!
logLine: aLineToLog
	
	log nextPutAll: aLineToLog; newLine.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Logger class' category: 'MarsRover-WithHeading'!
Logger class
	instanceVariableNames: ''!

!Logger class methodsFor: 'instance creation' stamp: 'AT 10/27/2021 21:21:47'!
for: aMarsRover 

	^self new initializeFor: aMarsRover ! !


!classDefinition: #LoggerChangeReceptor category: 'MarsRover-WithHeading'!
Object subclass: #LoggerChangeReceptor
	instanceVariableNames: 'logger marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!LoggerChangeReceptor methodsFor: 'does not understand' stamp: 'AT 10/28/2021 15:45:29'!
doesNotUnderstand: aMessage

	^ aMessage sendTo: logger.! !


!LoggerChangeReceptor methodsFor: 'initialization' stamp: 'AT 10/28/2021 14:42:32'!
initializeOn: aLogger for: aMarsRover
	
	logger _ aLogger.
	marsRover _ aMarsRover.! !


!classDefinition: #HeadLogger category: 'MarsRover-WithHeading'!
LoggerChangeReceptor subclass: #HeadLogger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!HeadLogger methodsFor: 'reception' stamp: 'AT 10/28/2021 15:40:19'!
updateChange: aNewHead 
	
	logger logLine: aNewHead printString.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HeadLogger class' category: 'MarsRover-WithHeading'!
HeadLogger class
	instanceVariableNames: ''!

!HeadLogger class methodsFor: 'instance creation' stamp: 'AT 10/28/2021 13:49:09'!
on: aLogger for: aMarsRover

	| logger |
	
	logger _ self new initializeOn: aLogger for: aMarsRover.
	
	aMarsRover notifyHeadChangesTo: logger.
	
	^ logger.
	
	! !



!classDefinition: #PositionLogger category: 'MarsRover-WithHeading'!
LoggerChangeReceptor subclass: #PositionLogger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!PositionLogger methodsFor: 'reception' stamp: 'AT 10/28/2021 15:40:19'!
updateChange: aNewPosition
	
	 logger logLine: aNewPosition printString! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PositionLogger class' category: 'MarsRover-WithHeading'!
PositionLogger class
	instanceVariableNames: ''!

!PositionLogger class methodsFor: 'instance creation' stamp: 'AT 10/28/2021 14:39:55'!
on: aLogger for: aMarsRover

	| logger |
	
	logger _ self new initializeOn: aLogger for: aMarsRover.
	
	aMarsRover notifyPositionChangesTo: logger.
	
	^ logger.! !


!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head changeBroadcaster'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'AT 10/28/2021 15:57:57'!
initializeAt: aPosition heading: aHeadingType

	position := aPosition.
	head := aHeadingType for: self.
	
	changeBroadcaster _ ChangeBroadcaster new.! !


!MarsRover methodsFor: 'heading' stamp: 'jpm 10/27/2021 15:33:36'!
headEast
	
	self headTo: MarsRoverHeadingEast.! !

!MarsRover methodsFor: 'heading' stamp: 'jpm 10/27/2021 15:34:05'!
headNorth
	
	self headTo: MarsRoverHeadingNorth.! !

!MarsRover methodsFor: 'heading' stamp: 'jpm 10/27/2021 15:34:20'!
headSouth
	
	self headTo: MarsRoverHeadingSouth.! !

!MarsRover methodsFor: 'heading' stamp: 'AT 10/28/2021 16:00:22'!
headTo: aNewHead
	
	head := aNewHead for: self.
	changeBroadcaster updateHead: head.
! !

!MarsRover methodsFor: 'heading' stamp: 'jpm 10/27/2021 15:34:35'!
headWest
	
	self headTo: MarsRoverHeadingWest.! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	head moveBackward! !

!MarsRover methodsFor: 'moving' stamp: 'jpm 10/26/2021 22:04:17'!
moveEast
	
	self moveTo: (1@0).! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	head moveForward! !

!MarsRover methodsFor: 'moving' stamp: 'jpm 10/26/2021 22:03:43'!
moveNorth
	
	self moveTo: (0@1).! !

!MarsRover methodsFor: 'moving' stamp: 'jpm 10/26/2021 22:03:24'!
moveSouth
	
	self moveTo: (0@-1).! !

!MarsRover methodsFor: 'moving' stamp: 'AT 10/28/2021 16:00:02'!
moveTo: aVector

	position := position + aVector.
	changeBroadcaster updatePosition: position.! !

!MarsRover methodsFor: 'moving' stamp: 'jpm 10/26/2021 22:03:02'!
moveWest
	
	self moveTo: (-1@0).! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'tracking' stamp: 'AT 10/28/2021 15:59:27'!
notifyHeadChangesTo: aHeadChangeReceptor

	changeBroadcaster notifyHeadChangesTo: aHeadChangeReceptor. ! !

!MarsRover methodsFor: 'tracking' stamp: 'AT 10/28/2021 15:59:32'!
notifyPositionChangesTo: aPositionChangeReceptor

	changeBroadcaster notifyPositionChangesTo: aPositionChangeReceptor. ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'printing' stamp: 'jpm 10/27/2021 15:39:24'!
printOn: aStream
	
	self subclassResponsibility.! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!MarsRoverHeadingEast methodsFor: 'printing' stamp: 'jpm 10/27/2021 15:39:44'!
printOn: aStream

	aStream nextPutAll: 'East'.! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'jpm 10/27/2021 15:38:36'!
rotateRight
	
	^marsRover headEast! !


!MarsRoverHeadingNorth methodsFor: 'printing' stamp: 'jpm 10/27/2021 15:39:57'!
printOn: aStream

	aStream nextPutAll: 'North'.! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!MarsRoverHeadingSouth methodsFor: 'printing' stamp: 'jpm 10/27/2021 15:40:05'!
printOn: aStream

	aStream nextPutAll: 'South'.! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!MarsRoverHeadingWest methodsFor: 'printing' stamp: 'jpm 10/27/2021 15:40:12'!
printOn: aStream

	aStream nextPutAll: 'West'.! !


!classDefinition: #Window category: 'MarsRover-WithHeading'!
Object subclass: #Window
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Window methodsFor: 'accessing' stamp: 'AT 10/28/2021 13:41:28'!
addField: aNewField with: aString 
	
	contents add: aNewField->aString.! !

!Window methodsFor: 'accessing' stamp: 'AT 10/28/2021 13:07:38'!
atField: aFieldToRead
	
	^ contents at: aFieldToRead.! !

!Window methodsFor: 'accessing' stamp: 'AT 10/28/2021 13:09:09'!
atField: aFieldToEdit put: aString 
	
	contents at: aFieldToEdit put: aString.! !


!Window methodsFor: 'initialization' stamp: 'AT 10/28/2021 13:04:50'!
initialize

	contents _ Dictionary new.! !


!classDefinition: #WindowChangeReceptor category: 'MarsRover-WithHeading'!
Object subclass: #WindowChangeReceptor
	instanceVariableNames: 'marsRover window'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!WindowChangeReceptor methodsFor: 'initialization' stamp: 'AT 10/28/2021 14:36:14'!
initializeOn: aWindow for: aMarsRover 
	
	window _ aWindow.
	marsRover _ aMarsRover.! !


!WindowChangeReceptor methodsFor: 'does not understand' stamp: 'AT 10/28/2021 15:46:18'!
doesNotUnderstand: aMessage

	^ aMessage sendTo: window.! !


!classDefinition: #WindowHeadReceptor category: 'MarsRover-WithHeading'!
WindowChangeReceptor subclass: #WindowHeadReceptor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!WindowHeadReceptor methodsFor: 'reception' stamp: 'AT 10/28/2021 15:40:19'!
updateChange: aNewHead 
	
	window atField: #Head put: aNewHead printString.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'WindowHeadReceptor class' category: 'MarsRover-WithHeading'!
WindowHeadReceptor class
	instanceVariableNames: ''!

!WindowHeadReceptor class methodsFor: 'instance creation' stamp: 'AT 10/28/2021 13:55:36'!
on: aWindow for: aMarsRover 
	
	| window |

	aWindow addField: #Head with: 'N/A'.

	window _ self new initializeOn: aWindow for: aMarsRover.
	
	aMarsRover notifyHeadChangesTo: window.
	
	^ window.! !


!classDefinition: #WindowPositionReceptor category: 'MarsRover-WithHeading'!
WindowChangeReceptor subclass: #WindowPositionReceptor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!WindowPositionReceptor methodsFor: 'reception' stamp: 'AT 10/28/2021 15:40:19'!
updateChange: aNewPosition 
	
	window atField: #Position put: aNewPosition printString.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'WindowPositionReceptor class' category: 'MarsRover-WithHeading'!
WindowPositionReceptor class
	instanceVariableNames: ''!

!WindowPositionReceptor class methodsFor: 'instance creation' stamp: 'AT 10/28/2021 13:55:06'!
on: aWindow for: aMarsRover

	| window |

	aWindow addField: #Position with: 'N/A'.

	window _ self new initializeOn: aWindow for: aMarsRover.
	
	aMarsRover notifyPositionChangesTo: window.
	
	^ window.! !
