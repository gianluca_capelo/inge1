!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'Min 9/20/2021 16:48:21'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack emptyStackHasNoTopErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'Min 9/20/2021 16:48:21'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack emptyStackHasNoTopErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'Min 9/20/2021 16:48:21'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack emptyStackHasNoTopErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'set up' stamp: 'jpm 9/19/2021 14:00:32'!
collectionToOOStack: aSentenceCollection
	
	| anOOStack |
	
	anOOStack _ OOStack new . 
	
	aSentenceCollection do: [ :sentence | anOOStack push: sentence].
	
	^anOOStack ! !


!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:48:18'!
test01CanNotFindByEmptyPrefix

	| anEmptyString |
	
	anEmptyString _ ''.
	
	self 
		should: [ SentenceFinderByPrefix with: anEmptyString .]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: SentenceFinderByPrefix emptyPrefixErrorDescription].
	

	
	
	

	

	

	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:48:10'!
test02CanNotFindByPrefixWithSpaces

	| aSpacedString |
	
	aSpacedString _ 'Ra mon'.	
	
	self 
		should: [ SentenceFinderByPrefix with: aSpacedString .]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: SentenceFinderByPrefix prefixWithWhiteSpacesErrorDescription].
	

	
	
	

	

	

	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:55:19'!
test03CanNotFindInInvalidSentenceSource

	| aPrefix aSentenceFinderByPrefix aSourceWithInvalidContents aSourceOfInvalidType |
	
	aPrefix _ 'Ra'.	
	aSentenceFinderByPrefix _ SentenceFinderByPrefix with: aPrefix .	
	aSourceOfInvalidType _ OrderedCollection with: 'Roma'.
	aSourceWithInvalidContents _ self collectionToOOStack: (OrderedCollection with: 5).
	
	self 
		should: [aSentenceFinderByPrefix findIn: aSourceOfInvalidType]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: SentenceFinderByPrefix invalidSentenceSourceErrorDescription].
		
	self 
		should: [aSentenceFinderByPrefix findIn: aSourceWithInvalidContents]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: SentenceFinderByPrefix invalidSentenceSourceErrorDescription].
	
	

	
	
	

	

	

	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:56:58'!
test04FindingInEmptyOOStackReturnsNoMatches

	| anEmptyStack aPrefix |
	
	aPrefix _ 'Romeo'.
	anEmptyStack _ OOStack new.
	
	self assert: anEmptyStack hasNoMatchesWith: aPrefix.

	

	
	
	

	

	

	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:58:17'!
test05FindingInOOStackWithNoMatchesReturnsNoMatches

	| anOOStackOfSentences aPrefix sentences |
	
	aPrefix _ 'Pedro'.
	sentences _ OrderedCollection  with: 'Jose' with: 'Tito' with: 'Ramon'.
	anOOStackOfSentences _ self collectionToOOStack: sentences.
	
	self assert: anOOStackOfSentences hasNoMatchesWith: aPrefix.
	
	
	

	

	

	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:58:26'!
test06FindingInOOStackWithOnlyOneMatchReturnsExclusiveMatch

	| aSentenceFinderByPrefix anOOStackOfSentences aPrefix sentences matchingSentences |
	
	aPrefix _ 'Ramon'.
	aSentenceFinderByPrefix _ SentenceFinderByPrefix with: aPrefix .
	sentences _ OrderedCollection with: 'Ramon' .
	anOOStackOfSentences _ self collectionToOOStack: sentences.
	
	matchingSentences _ aSentenceFinderByPrefix findIn: anOOStackOfSentences.
	
	self assert: matchingSentences size = 1.
	self assert: (matchingSentences includes: 'Ramon').! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:58:32'!
test07FindingInOOStackWithMultipleMatchesReturnsAllOfThem

	| aSentenceFinderByPrefix anOOStackOfSentences aPrefix sentences actualMatchingSentences expectedMatchingSentences |
	
	aPrefix _ 'Ram'.
	aSentenceFinderByPrefix _ SentenceFinderByPrefix with: aPrefix .
	sentences _ OrderedCollection  with: 'Ramen' with: 'Roma' with: 'Ramon'.
	anOOStackOfSentences _ self collectionToOOStack: sentences.
	
	actualMatchingSentences _ aSentenceFinderByPrefix findIn: anOOStackOfSentences.
	expectedMatchingSentences _ OrderedCollection  with: 'Ramen' with: 'Ramon'.
	
	self assert: expectedMatchingSentences containsSameElementsAs: actualMatchingSentences.! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:58:38'!
test08FindingInOOStackWithRepeatedMatchesReturnsAllRepetitions

	| aSentenceFinderByPrefix anOOStackOfSentences aPrefix sentences actualMatchingSentences expectedMatchingSentences |
	
	aPrefix _ 'Ram'.
	aSentenceFinderByPrefix _ SentenceFinderByPrefix with: aPrefix .
	sentences _ OrderedCollection  with: 'Ramen' with: 'Ramen' with: 'Roma' with: 'Ramon'.
	anOOStackOfSentences _ self collectionToOOStack: sentences.
	
	actualMatchingSentences _ aSentenceFinderByPrefix findIn: anOOStackOfSentences.
	expectedMatchingSentences _ OrderedCollection  with: 'Ramen' with: 'Ramen' with: 'Ramon'.
	
	self assert: expectedMatchingSentences containsSameElementsAs: actualMatchingSentences.! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:58:48'!
test09FinderHandlesCaseSensitivityCorrectly

	| matchingSentences aSentenceFinderByPrefix anOOStackOfSentences aPrefix sentences |
	
	aPrefix _ 'pe'.
	aSentenceFinderByPrefix _ SentenceFinderByPrefix with: aPrefix .
	sentences _ OrderedCollection  with: 'Pe' with: 'pE' with: 'pe'.
	anOOStackOfSentences _ self collectionToOOStack: sentences.
	
	matchingSentences _ aSentenceFinderByPrefix findIn: anOOStackOfSentences.
	
	self assert: matchingSentences size = 1.
	self assert: matchingSentences includes: 'pe'.

	
	
	

	

	

	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'Min 9/20/2021 15:58:57'!
test10FinderDoesNotModifyOOStackContentsAndOrder

	| aSentenceFinderByPrefix anOOStackOfSentences aPrefix sentences |
	
	aPrefix _ 'Ram'.
	aSentenceFinderByPrefix _ SentenceFinderByPrefix with: aPrefix .
	sentences _ OrderedCollection  with: 'Ramen' with: 'Roma' with: 'Ramon'.
	anOOStackOfSentences _ self collectionToOOStack: sentences.
	
	aSentenceFinderByPrefix findIn: anOOStackOfSentences.
	
	self assert: anOOStackOfSentences hasSameOrderAndElementsAs: sentences.
	! !


!SentenceFinderByPrefixTest methodsFor: 'assertions' stamp: 'Min 9/19/2021 19:58:44'!
assert: anOrderedCollection contains: anotherOrderedCollection

	self assert: (anotherOrderedCollection allSatisfy: [:element | anOrderedCollection includes: element]).! !

!SentenceFinderByPrefixTest methodsFor: 'assertions' stamp: 'Min 9/19/2021 19:58:09'!
assert: anOrderedCollection containsSameElementsAs: anotherOrderedCollection
	
	self assert: anOrderedCollection contains: anotherOrderedCollection.
	self assert: anotherOrderedCollection contains: anOrderedCollection.! !

!SentenceFinderByPrefixTest methodsFor: 'assertions' stamp: 'Min 9/20/2021 15:59:08'!
assert: anOOStackOfSentences hasNoMatchesWith: aPrefix
	
	| aSentenceFinderByPrefix matchingSentences |

	aSentenceFinderByPrefix _ SentenceFinderByPrefix with: aPrefix .
	matchingSentences _ aSentenceFinderByPrefix findIn: anOOStackOfSentences.
	
	self assert: matchingSentences isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'assertions' stamp: 'jpm 9/19/2021 11:04:45'!
assert: anOOStack hasSameOrderAndElementsAs: anOrderedCollection
	

	self assert: anOOStack size equals: anOrderedCollection size.
	
	[anOOStack isEmpty not] whileTrue: [
		self assert: anOOStack pop equals: anOrderedCollection removeLast.
	].! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'testing' stamp: 'GC 9/18/2021 14:49:46'!
isEmpty
	
	^ self size = 0.! !


!OOStack methodsFor: 'accesing' stamp: 'GC 9/18/2021 18:50:24'!
pop 

	
	^ (OOStackHandler for: self) handlePop: self
	! !

!OOStack methodsFor: 'accesing' stamp: 'Min 9/19/2021 21:35:30'!
push: anElement
	
	contents add: anElement .
	
	! !

!OOStack methodsFor: 'accesing' stamp: 'Min 9/19/2021 21:35:30'!
size 

	^contents size.! !

!OOStack methodsFor: 'accesing' stamp: 'GC 9/18/2021 18:26:33'!
top

	^(OOStackHandler for: self) handleTop: self.! !


!OOStack methodsFor: 'private' stamp: 'Min 9/19/2021 21:35:30'!
last
	^contents last! !

!OOStack methodsFor: 'private' stamp: 'Min 9/19/2021 21:35:30'!
removeLast
	
	^contents removeLast.! !


!OOStack methodsFor: 'initialization' stamp: 'Min 9/19/2021 21:35:30'!
initialize
	contents _ OrderedCollection new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'Min 9/20/2021 16:48:21'!
emptyStackHasNoTopErrorDescription
	
	^ 'An empty stack does not have an element at the top!!!!!!'.! !


!classDefinition: #OOStackHandler category: 'Stack-Exercise'!
Object subclass: #OOStackHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackHandler methodsFor: 'event handling' stamp: 'GC 9/18/2021 18:29:24'!
handlePop: anOOStack
	self subclassResponsibility.! !

!OOStackHandler methodsFor: 'event handling' stamp: 'GC 9/18/2021 18:29:20'!
handleTop: anOOStack
	self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStackHandler class' category: 'Stack-Exercise'!
OOStackHandler class
	instanceVariableNames: ''!

!OOStackHandler class methodsFor: 'as yet unclassified' stamp: 'GC 9/18/2021 18:34:21'!
for: aStack
	
	^ (self subclasses detect: [: subclass | subclass isFor: aStack ] ) new! !

!OOStackHandler class methodsFor: 'as yet unclassified' stamp: 'GC 9/18/2021 18:26:58'!
isFor
	self subclassResponsibility .! !


!classDefinition: #EmptyStackHandler category: 'Stack-Exercise'!
OOStackHandler subclass: #EmptyStackHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!EmptyStackHandler methodsFor: 'event handling' stamp: 'Min 9/20/2021 16:48:21'!
handlePop: anOOStack

	^anOOStack error: anOOStack class emptyStackHasNoTopErrorDescription .! !

!EmptyStackHandler methodsFor: 'event handling' stamp: 'Min 9/20/2021 16:48:21'!
handleTop: anOOStack
	
	^ anOOStack error: anOOStack class emptyStackHasNoTopErrorDescription .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EmptyStackHandler class' category: 'Stack-Exercise'!
EmptyStackHandler class
	instanceVariableNames: ''!

!EmptyStackHandler class methodsFor: 'as yet unclassified' stamp: 'GC 9/18/2021 18:07:28'!
isFor: aStack

	^ aStack isEmpty .! !


!classDefinition: #NonEmptyStackHandler category: 'Stack-Exercise'!
OOStackHandler subclass: #NonEmptyStackHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!NonEmptyStackHandler methodsFor: 'event handling' stamp: 'GC 9/18/2021 18:23:48'!
handlePop: aOOStack
	
	^aOOStack removeLast.! !

!NonEmptyStackHandler methodsFor: 'event handling' stamp: 'jpm 9/19/2021 12:20:26'!
handleTop: aOOStack
	
	^aOOStack last.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NonEmptyStackHandler class' category: 'Stack-Exercise'!
NonEmptyStackHandler class
	instanceVariableNames: ''!

!NonEmptyStackHandler class methodsFor: 'as yet unclassified' stamp: 'GC 9/18/2021 18:07:42'!
isFor: aStack

	^ aStack isEmpty not.! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'prefix'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'find operations' stamp: 'Min 9/20/2021 15:38:38'!
findIn: aSentenceSource
		
	(self sourceIsOOStackOfSentences: aSentenceSource) ifTrue: [^self findInOOStack: aSentenceSource].
	
	^ self error: self class invalidSentenceSourceErrorDescription.! !

!SentenceFinderByPrefix methodsFor: 'find operations' stamp: 'Min 9/20/2021 16:00:01'!
findInOOStack: aSentenceSource
		
	| topSentence matchingSentences |
	
	aSentenceSource isEmpty ifTrue: [ ^OrderedCollection new. ].
	
	
	topSentence _ aSentenceSource pop.
	
	matchingSentences _ self findIn: aSentenceSource.
	
	aSentenceSource push: topSentence.
	
	(topSentence beginsWith: prefix) ifTrue: [ matchingSentences add: topSentence. ].

	^matchingSentences. ! !


!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'Min 9/19/2021 21:40:18'!
initializeWith: aPrefix

	prefix _ aPrefix .! !


!SentenceFinderByPrefix methodsFor: 'source identification' stamp: 'Min 9/20/2021 16:52:27'!
ooStackOnlyContainsSentences: aSourceOOStack

	| examinedElements sourceContentsAreSentences |
	
	sourceContentsAreSentences _ true.
	examinedElements _ OOStack new.
	
	[aSourceOOStack isEmpty] whileFalse: [ 
		| sourceTop |
		
		sourceTop _ aSourceOOStack pop.
		sourceContentsAreSentences _ sourceContentsAreSentences and: [sourceTop isKindOf: String].
		examinedElements push: sourceTop.
		].
	
	[examinedElements isEmpty] whileFalse: [aSourceOOStack push: examinedElements pop].
	
	^ sourceContentsAreSentences.! !

!SentenceFinderByPrefix methodsFor: 'source identification' stamp: 'Min 9/20/2021 16:49:32'!
sourceIsOOStackOfSentences: anUnidentifiedSenteceSource
	
	| sourceIsOOStack |
	
	sourceIsOOStack _ anUnidentifiedSenteceSource isKindOf: OOStack.
	
	^ sourceIsOOStack and: [self ooStackOnlyContainsSentences: anUnidentifiedSenteceSource].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'instance creation' stamp: 'Min 9/20/2021 15:11:57'!
with: aStringToBeUsedAsPrefix

	self assertIsAPrefix: aStringToBeUsedAsPrefix .
	
	^ self new initializeWith: aStringToBeUsedAsPrefix .

! !


!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'Min 9/20/2021 15:04:24'!
emptyPrefixErrorDescription

	^'Prefix must not be empty!!!!!!'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'Min 9/20/2021 15:39:45'!
invalidSentenceSourceErrorDescription

	^'Invalid source of sentences!!!!!!'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'Min 9/20/2021 15:11:34'!
prefixIsNotStringErrorDescription
	
	^'Prefix must be a String!!!!!!'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'Min 9/20/2021 15:09:20'!
prefixWithWhiteSpacesErrorDescription

	^'Prefix must not have white spaces!!!!!!'! !


!SentenceFinderByPrefix class methodsFor: 'assertions' stamp: 'Min 9/20/2021 15:12:59'!
assertIsAPrefix: aString

	(aString isKindOf: String) ifFalse: [self error: self prefixIsNotStringErrorDescription].
	
	aString isEmpty ifTrue: [self error: self emptyPrefixErrorDescription].
	
	(aString includesSubString: ' ') ifTrue: [self error: self prefixWithWhiteSpacesErrorDescription].
! !
