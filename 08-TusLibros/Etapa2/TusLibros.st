!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:42:42'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:42:49'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectFactory createCart.
	
	self 
		should: [ cart add: testObjectFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:44:36'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectFactory createCart.
	
	cart add: testObjectFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:44:43'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:44:47'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:44:52'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectFactory createCart.
	
	cart add: testObjectFactory itemSellByTheStore.
	self assert: (cart includes: testObjectFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:44:57'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectFactory createCart.
	
	self deny: (cart includes: testObjectFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'TT 11/4/2021 16:45:02'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectFactory createCart.
	
	cart add: 2 of: testObjectFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectFactory itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'TT 11/4/2021 16:44:09'!
initialize

	testObjectFactory _ TestObjectFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'setup' stamp: 'TT 11/4/2021 16:53:03'!
initialize

	testObjectFactory _ TestObjectFactory new.! !


!CashierTest methodsFor: 'assertions' stamp: 'TT 11/4/2021 16:33:47'!
shouldCheckOut: aCart payingWith: aCreditCard onDate: aDate registeringOn: aRegisterBook debitingWith: aMerchantProcessorInterface failWithDescription: anErrorDescription

	| registerBookBeforeCheckOut |
	
	registerBookBeforeCheckOut _ aRegisterBook copy.
	
	self 
		should: [ 	(CheckoutCashier 
					for: aCart 
					payingWith: aCreditCard
					onDate: aDate
					registeringOn: aRegisterBook 
					debitingWith: aMerchantProcessorInterface) 
				checkOut .]
		raise: Error
		withExceptionDo: [ :anException | 
						self assert: anErrorDescription equals: anException messageText. 
						self assert: registerBookBeforeCheckOut equals: aRegisterBook.
						].! !


!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:53:18'!
test01CanNotCheckOutWithEmptyCart
				
	self 
		shouldCheckOut: testObjectFactory createCart
		payingWith: testObjectFactory validCard  
		onDate: Date today 
		registeringOn: testObjectFactory emptyRegisterBook 
		debitingWith: testObjectFactory validMP 
		failWithDescription: CheckoutCashier canNotCheckOutWithEmptyCartErrorDescription.
	! !

!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:53:29'!
test02CanCheckOutWithValidCard

	| cart aProduct anotherProduct price registerBook |
	
	cart _ testObjectFactory createCart.
	aProduct _ testObjectFactory aValidBook.
	anotherProduct _ testObjectFactory anotherValidBook.
	cart add: aProduct; add: 2 of: anotherProduct.
	registerBook _ testObjectFactory emptyRegisterBook.
	
	price _ (CheckoutCashier 
				for: cart 
				payingWith: testObjectFactory validCard 
				onDate: Date today 
				registeringOn: registerBook 
				debitingWith: testObjectFactory validMP) 
			checkOut.	
	
	self assert: 80 equals: price.
	self assert: 3 equals: registerBook size.
	self assert: 1 equals: (registerBook occurrencesOf: aProduct). 
	self assert: 2 equals: (registerBook occurrencesOf: anotherProduct). 
	
	
		! !

!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:53:35'!
test03CanNotCheckOutWithExpiredCard

	self 
		shouldCheckOut: testObjectFactory aCartWithValidBook
		payingWith: testObjectFactory expiredCard  
		onDate: Date today 
		registeringOn: testObjectFactory emptyRegisterBook 
		debitingWith: testObjectFactory validMP 
		failWithDescription: CheckoutCashier expiredCreditCardErrorDescription.
	
	
		! !

!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:53:41'!
test04CanNotCheckOutWithInvalidCardNumber

	self 
		shouldCheckOut: testObjectFactory aCartWithValidBook
		payingWith: testObjectFactory invalidDigitAmountCreditCard  
		onDate: Date today 
		registeringOn: testObjectFactory emptyRegisterBook 
		debitingWith: testObjectFactory validMP 
		failWithDescription: CheckoutCashier invalidDigitAmountErrorDescription.
	
	
		! !

!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:53:48'!
test05CanNotCheckOutWithEmptyOwnerName

	self 
		shouldCheckOut: testObjectFactory aCartWithValidBook
		payingWith: testObjectFactory emptyOwnerNameCreditCard  
		onDate: Date today 
		registeringOn: testObjectFactory emptyRegisterBook 
		debitingWith: testObjectFactory validMP 
		failWithDescription: CheckoutCashier invalidOwnerNameCreditCardErrorDescriprion.
	
		! !

!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:53:55'!
test06CanNotCheckOutWithOwnerNameLongerThanAllowed

	self 
		shouldCheckOut: testObjectFactory aCartWithValidBook
		payingWith: testObjectFactory longerThanAllowedOwnerNameCreditCard  
		onDate: Date today 
		registeringOn: testObjectFactory emptyRegisterBook 
		debitingWith: testObjectFactory validMP 
		failWithDescription: CheckoutCashier invalidOwnerNameCreditCardErrorDescriprion.
	
		! !

!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:54:00'!
test07CanNotCheckOutWithoutEnoughFunds

	self 
		shouldCheckOut: testObjectFactory aCartWithValidBook
		payingWith: testObjectFactory validCard  
		onDate: Date today 
		registeringOn: testObjectFactory emptyRegisterBook 
		debitingWith: testObjectFactory mpWhenCheckingOutWithoutFunds 
		failWithDescription: CheckoutCashier notEnoughFundsOnCreditCardErrorDescriprion.! !

!CashierTest methodsFor: 'testing' stamp: 'TT 11/4/2021 16:54:17'!
test08CanNotCheckOutWithStolenCard

	self 
		shouldCheckOut: testObjectFactory aCartWithValidBook
		payingWith: testObjectFactory validCard  
		onDate: Date today 
		registeringOn: testObjectFactory emptyRegisterBook 
		debitingWith: testObjectFactory mpWhenCheckingOutWithStolenCard 
		failWithDescription: CheckoutCashier stolenCreditCardErrorDescription.! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalogue contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'TT 11/1/2021 20:19:16'!
assertIsValidItem: anItem

	(catalogue includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'TT 11/1/2021 20:19:16'!
initializeAcceptingItemsOf: aCatalog

	catalogue := aCatalog.
	contents := Bag new.! !


!Cart methodsFor: 'queries' stamp: 'TT 11/1/2021 19:28:05'!
occurrencesOf: anItem

	^contents occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'TT 11/1/2021 19:28:05'!
includes: anItem

	^contents includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'TT 11/1/2021 19:28:05'!
isEmpty
	
	^contents isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'TT 11/1/2021 19:28:52'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	contents add: anItem withOccurrences: aQuantity.! !


!Cart methodsFor: 'checking out' stamp: 'TT 11/2/2021 19:45:12'!
registerProductsOn: aRegisterBook

	aRegisterBook addAll: contents. 
	
	! !

!Cart methodsFor: 'checking out' stamp: 'TT 11/2/2021 19:25:05'!
subtotal
	
	^ contents sum: [ :aProduct | catalogue at: aProduct. ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #CheckoutCashier category: 'TusLibros'!
Object subclass: #CheckoutCashier
	instanceVariableNames: 'cart creditCard registerBook checkOutDate merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CheckoutCashier methodsFor: 'checking out' stamp: 'TT 11/3/2021 18:38:55'!
checkOut

	| subtotal |
	
	subtotal _ cart subtotal.
	
	merchantProcessor debit: subtotal from: creditCard.
	
	cart registerProductsOn: registerBook.
		
	^ subtotal.
	
	! !


!CheckoutCashier methodsFor: 'initialization' stamp: 'TT 11/4/2021 16:35:21'!
for: aCart payingWith: aCreditCard onDate: aDayAndMonth registeringOn: aRegisterBook debitingWith: aMerchantProcessor   
	
	cart _ aCart.
	creditCard _ aCreditCard.
	checkOutDate  _ aDayAndMonth.
	registerBook _ aRegisterBook.
	merchantProcessor _ aMerchantProcessor.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CheckoutCashier class' category: 'TusLibros'!
CheckoutCashier class
	instanceVariableNames: ''!

!CheckoutCashier class methodsFor: 'raising error' stamp: 'TT 11/2/2021 20:15:48'!
signalCanNotCheckOutWithEmptyCartError

	^ self error: self canNotCheckOutWithEmptyCartErrorDescription! !

!CheckoutCashier class methodsFor: 'raising error' stamp: 'TT 11/2/2021 20:17:10'!
signalExpiredCreditCardError

	^ self error: self expiredCreditCardErrorDescription! !


!CheckoutCashier class methodsFor: 'error descriptions' stamp: 'TT 11/1/2021 19:43:13'!
canNotCheckOutWithEmptyCartErrorDescription

	^ 'No se puede hacer checkout con un carrito vacio'! !

!CheckoutCashier class methodsFor: 'error descriptions' stamp: 'TT 11/2/2021 20:11:53'!
expiredCreditCardErrorDescription
	
	^ 'Tried to check out with an expired credit card!!!!!!'.! !

!CheckoutCashier class methodsFor: 'error descriptions' stamp: 'TT 11/2/2021 20:33:38'!
invalidDigitAmountErrorDescription
	
	^ 'Tried to check out with a Credit Card whose number does not have 16 digits'.! !

!CheckoutCashier class methodsFor: 'error descriptions' stamp: 'TT 11/2/2021 20:51:17'!
invalidOwnerNameCreditCardErrorDescriprion
	
	^ 'Tried to check out with an credit card whose owner name is invalid!!!!!!'.! !

!CheckoutCashier class methodsFor: 'error descriptions' stamp: 'TT 11/2/2021 21:52:39'!
notEnoughFundsOnCreditCardErrorDescriprion
	
	^ 'Tried to check out without enough funds.'! !

!CheckoutCashier class methodsFor: 'error descriptions' stamp: 'TT 11/2/2021 22:01:21'!
stolenCreditCardErrorDescription
	
	^ 'Tried to check out with a stolen credit card'.! !


!CheckoutCashier class methodsFor: 'assertions' stamp: 'TT 11/4/2021 16:58:04'!
assert: aCreditCard isNotExpiredOn: aDayAndMonth

	^ aCreditCard expirationDate < aDayAndMonth ifTrue: [ self signalExpiredCreditCardError. ]! !

!CheckoutCashier class methodsFor: 'assertions' stamp: 'TT 11/4/2021 16:57:03'!
assertCartNotEmpty: aCart

	^ aCart isEmpty ifTrue: [ self signalCanNotCheckOutWithEmptyCartError. ]! !

!CheckoutCashier class methodsFor: 'assertions' stamp: 'TT 11/4/2021 16:58:31'!
assertValidCard: aCreditCard payingOn: aDayAndMonth

	self assert: aCreditCard isNotExpiredOn: aDayAndMonth.
	
	self assertValidNumber: aCreditCard.
	
	self assertValidOwnerName: aCreditCard! !

!CheckoutCashier class methodsFor: 'assertions' stamp: 'TT 11/4/2021 16:58:18'!
assertValidNumber: aCreditCard

	^ aCreditCard number decimalDigitLength ~= 16 ifTrue: [ self error: self invalidDigitAmountErrorDescription. ]! !

!CheckoutCashier class methodsFor: 'assertions' stamp: 'TT 11/4/2021 16:58:31'!
assertValidOwnerName: aCreditCard

	^ (aCreditCard ownerName size between: 1 and: 30) ifFalse: [ self error: self invalidOwnerNameCreditCardErrorDescriprion. ]! !


!CheckoutCashier class methodsFor: 'instance creation' stamp: 'TT 11/4/2021 16:57:03'!
for: aCart payingWith: aCreditCard onDate: aDayAndMonth registeringOn: aRegisterBook debitingWith: aMerchantProcessor  

	self assertCartNotEmpty: aCart.
	
	self assertValidCard: aCreditCard payingOn: aDayAndMonth.

	^self new 
		for: aCart 
		payingWith: aCreditCard 
		onDate: aDayAndMonth 
		registeringOn: aRegisterBook 
		debitingWith: aMerchantProcessor.! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'number expirationDate owner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'TT 11/4/2021 16:39:53'!
initializeWithId: a16DigitNumber expiringOn: aDate belongingTo: anOwnerName  
	
	number _ a16DigitNumber.
	expirationDate _ aDate.
	owner _ anOwnerName.! !


!CreditCard methodsFor: 'as yet unclassified' stamp: 'TT 11/2/2021 20:28:01'!
expirationDate
	
	^ expirationDate.! !

!CreditCard methodsFor: 'as yet unclassified' stamp: 'TT 11/2/2021 20:32:15'!
number
	
	^number! !

!CreditCard methodsFor: 'as yet unclassified' stamp: 'TT 11/2/2021 20:48:58'!
ownerName
	
	^ owner.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'TT 11/4/2021 16:39:53'!
withId: anId expiringOn: aMonthAndYear belongingTo: anOwnerName  

	^self new initializeWithId: anId expiringOn: aMonthAndYear belongingTo: anOwnerName.! !


!classDefinition: #MerchantProcessorInterfaceSimulator category: 'TusLibros'!
Object subclass: #MerchantProcessorInterfaceSimulator
	instanceVariableNames: 'answer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorInterfaceSimulator methodsFor: 'debiting from card' stamp: 'TT 11/4/2021 15:41:26'!
debit: aTransferAmount from: aCreditCard

	^ answer value: aTransferAmount value: aCreditCard.! !


!MerchantProcessorInterfaceSimulator methodsFor: 'initialization' stamp: 'TT 11/4/2021 16:40:05'!
initializeAnswering: aClosure 
	
	answer _ aClosure.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessorInterfaceSimulator class' category: 'TusLibros'!
MerchantProcessorInterfaceSimulator class
	instanceVariableNames: ''!

!MerchantProcessorInterfaceSimulator class methodsFor: 'instance creation' stamp: 'TT 11/4/2021 15:34:59'!
answering: aClosure 

	^self new initializeAnswering: aClosure ! !


!classDefinition: #TestObjectFactory category: 'TusLibros'!
Object subclass: #TestObjectFactory
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TestObjectFactory methodsFor: 'carts' stamp: 'TT 11/4/2021 16:45:44'!
aCartWithValidBook

	| cart product |
	
	cart _ self createCart.
	product _ self aValidBook.
	cart add: product.
	
	^ cart! !

!TestObjectFactory methodsFor: 'carts' stamp: 'TT 11/4/2021 16:41:13'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !


!TestObjectFactory methodsFor: 'catalogues' stamp: 'TT 11/4/2021 16:47:35'!
defaultCatalog
	
	^ Dictionary newFromPairs: {self itemSellByTheStore. 10. self aValidBook. 20. self anotherValidBook. 30. }! !


!TestObjectFactory methodsFor: 'credit cards' stamp: 'TT 11/4/2021 16:49:01'!
emptyOwnerNameCreditCard
	
	^ CreditCard withId: 1111222233334444 expiringOn: Date today belongingTo: ''. ! !

!TestObjectFactory methodsFor: 'credit cards' stamp: 'TT 11/4/2021 16:49:18'!
expiredCard
	
	^ CreditCard withId: 1111222233334444 expiringOn: Date yesterday belongingTo: 'Hasbulla'.! !

!TestObjectFactory methodsFor: 'credit cards' stamp: 'TT 11/4/2021 16:50:09'!
invalidDigitAmountCreditCard
	
	^ CreditCard withId: 1 expiringOn: Date today belongingTo: 'Hasbulla'.! !

!TestObjectFactory methodsFor: 'credit cards' stamp: 'TT 11/4/2021 16:50:19'!
longerThanAllowedOwnerNameCreditCard
	
	^ CreditCard withId: 1234123412341234 expiringOn: Date today belongingTo: 'Gianluca Capelo Juan Pablo Miceli'.! !

!TestObjectFactory methodsFor: 'credit cards' stamp: 'TT 11/4/2021 16:52:43'!
validCard

	^ CreditCard withId: 1111222233334444 expiringOn: Date today belongingTo: 'Hasbulla'.! !


!TestObjectFactory methodsFor: 'mp simulators' stamp: 'TT 11/4/2021 16:50:41'!
mpWhenCheckingOutWithStolenCard

	^ MerchantProcessorInterfaceSimulator answering: [ :aTransferAmount :aCreditCard | Error new signal: CheckoutCashier stolenCreditCardErrorDescription. ]! !

!TestObjectFactory methodsFor: 'mp simulators' stamp: 'TT 11/4/2021 16:50:56'!
mpWhenCheckingOutWithoutFunds

	^ MerchantProcessorInterfaceSimulator answering: [ :aTransferAmount :aCreditCard | Error new signal: CheckoutCashier  notEnoughFundsOnCreditCardErrorDescriprion. ]! !

!TestObjectFactory methodsFor: 'mp simulators' stamp: 'TT 11/4/2021 16:51:58'!
validMP

	^ MerchantProcessorInterfaceSimulator answering: [ :aTransferAmount :aCreditCard | ].! !


!TestObjectFactory methodsFor: 'register books' stamp: 'TT 11/4/2021 16:51:20'!
emptyRegisterBook

	^ Bag new! !


!TestObjectFactory methodsFor: 'products' stamp: 'TT 11/4/2021 16:47:59'!
aValidBook

	^ 'a valid book'.! !

!TestObjectFactory methodsFor: 'products' stamp: 'TT 11/4/2021 16:48:13'!
anotherValidBook

	^ 'another valid book'! !

!TestObjectFactory methodsFor: 'products' stamp: 'TT 11/4/2021 16:41:55'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TestObjectFactory methodsFor: 'products' stamp: 'TT 11/4/2021 16:41:35'!
itemSellByTheStore
	
	^ 'validBook'! !
