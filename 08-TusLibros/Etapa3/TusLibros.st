!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 18:09'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:50'!
test01CanNotCheckoutAnEmptyCart

	| salesBook |
	
	salesBook := OrderedCollection new.
	self 
		should: [ Cashier 
			toCheckout: testObjectsFactory createCart 
			charging: testObjectsFactory notExpiredCreditCard 
			throught: self
			on: testObjectsFactory today
			registeringOn:  salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier cartCanNotBeEmptyErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test02CalculatedTotalIsCorrect

	| cart cashier |
	
	cart := testObjectsFactory createCart.
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	
	cashier :=  Cashier
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard 
		throught: self
		on: testObjectsFactory today 
		registeringOn: OrderedCollection new.
		
	self assert: cashier checkOut = (testObjectsFactory itemSellByTheStorePrice * 2)! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test03CanNotCheckoutWithAnExpiredCreditCart

	| cart salesBook |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
	
	self
		should: [ Cashier 
				toCheckout: cart 
				charging: testObjectsFactory expiredCreditCard 
				throught: self
				on: testObjectsFactory today
				registeringOn: salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | 
			self assert: anError messageText = Cashier canNotChargeAnExpiredCreditCardErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:04'!
test04CheckoutRegistersASale

	| cart cashier salesBook total |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	total := cashier checkOut.
					
	self assert: salesBook size = 1.
	self assert: salesBook first total = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:00'!
test05CashierChargesCreditCardUsingMerchantProcessor

	| cart cashier salesBook total creditCard debitedAmout debitedCreditCard  |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	debitBehavior := [ :anAmount :aCreditCard | 
		debitedAmout := anAmount.
		debitedCreditCard := aCreditCard ].
	total := cashier checkOut.
					
	self assert: debitedCreditCard = creditCard.
	self assert: debitedAmout = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:59'!
test06CashierDoesNotSaleWhenTheCreditCardHasNoCredit

	| cart cashier salesBook creditCard |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 	debitBehavior := [ :anAmount :aCreditCard | self error: Cashier creditCardHasNoCreditErrorMessage].
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	self 
		should: [cashier checkOut ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier creditCardHasNoCreditErrorMessage.
			self assert: salesBook isEmpty ]! !


!CashierTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 19:03'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'merchant processor protocol' stamp: 'kh 11/6/2021 14:29:00'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #RestInterfaceTest category: 'TusLibros'!
TestCase subclass: #RestInterfaceTest
	instanceVariableNames: 'testObjectsFactory shouldAuthenticate actualTime debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 16:52:51'!
test01CanNotCreateCartForInvalidUser

	| interface creatingCart |
	
	shouldAuthenticate _ [ :aUser :aPassword | false ].
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.	
	creatingCart _ [ interface createCartFor: testObjectsFactory invalidUser with: testObjectsFactory validPassword ].
	
	self should: creatingCart 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :exception |			self assert: RestInterface invalidUserErrorDescription equals: exception messageText.			].! !

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 15:52:43'!
test02CanCreateCartForValidUser

	| interface cartId cartProducts |
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.
	
	cartId _ interface createCartFor: testObjectsFactory validUser with: testObjectsFactory validPassword.
	cartProducts _ interface listCart: cartId.
	
	self assert: cartProducts isEmpty.! !

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 16:44:00'!
test03CanNotOperateWithUnregisteredCart

	| interface  unregisteredCartId  checkingOut addingProduct listingCart |
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.
	unregisteredCartId _ 1.
	
	addingProduct _ [ interface addToCart: unregisteredCartId bookIsbn: testObjectsFactory itemSellByTheStore bookQuantity: 1 ].
	listingCart _ [ interface listCart: unregisteredCartId ].
	checkingOut _ [ interface 
					checkout: unregisteredCartId 
					withCreditCardNumber: testObjectsFactory validCreditCardNumber 
					withExpiringDate: testObjectsFactory notExpiredDate 
					withOwnerName: testObjectsFactory validOwnerName].


	self shouldRaiseUnregisteredCartErrorWhen:addingProduct.
	self shouldRaiseUnregisteredCartErrorWhen:listingCart.
	self shouldRaiseUnregisteredCartErrorWhen: checkingOut.
	
	! !

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 16:49:32'!
test04CanNotOperateWithExpiredCart

	| interface cartId addingProduct checkingOut listingCart |
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.
	
	cartId _ interface createCartFor: testObjectsFactory validUser with: testObjectsFactory validPassword .
	
	actualTime _ actualTime + (Duration minutes: 31).
	
	addingProduct _ [ interface addToCart: cartId bookIsbn: testObjectsFactory itemSellByTheStore bookQuantity: 1 ].
	listingCart _ [ interface listCart: cartId ].
	checkingOut _ [ interface 
					checkout: cartId 
					withCreditCardNumber: testObjectsFactory validCreditCardNumber 
					withExpiringDate: testObjectsFactory notExpiredDate 
					withOwnerName: testObjectsFactory validOwnerName].

	self shouldRaiseExpiredCartErrorWhen: addingProduct.
	self shouldRaiseExpiredCartErrorWhen: listingCart .
	self shouldRaiseExpiredCartErrorWhen: checkingOut .
! !

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 16:26:30'!
test05CanAddProductsToRegisteredCart

	| interface cartId cartProducts |
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.
	
	cartId _ interface createCartFor: testObjectsFactory validUser with: testObjectsFactory validPassword.
	interface addToCart: cartId bookIsbn: testObjectsFactory itemSellByTheStore bookQuantity: 1.
	cartProducts _ interface listCart: cartId.
	
	self assert: 1 equals: cartProducts size.
	self assert: 1 equals: (cartProducts occurrencesOf: testObjectsFactory itemSellByTheStore).! !

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 16:28:26'!
test06CanCheckOutRegisteredCart

	| interface cartId checkOutSale |
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.
	cartId _ interface createCartFor: testObjectsFactory validUser with: testObjectsFactory validPassword.
	interface addToCart: cartId bookIsbn: testObjectsFactory itemSellByTheStore bookQuantity: 1.
	
	checkOutSale _ interface checkout: cartId 
				withCreditCardNumber: testObjectsFactory validCreditCardNumber 
				withExpiringDate: testObjectsFactory notExpiredDate 
				withOwnerName: testObjectsFactory validOwnerName.
	
	
	self assert: testObjectsFactory itemSellByTheStorePrice equals: checkOutSale total.

	
	
! !

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 16:53:28'!
test07CanNotListPurchasesOfInvalidUser

	| interface listingPurchases |
	
	shouldAuthenticate _ [ :aUser :aPassword | false ].
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.		
	listingPurchases _ [ interface listPurchasesOf: testObjectsFactory invalidUser withPassword: testObjectsFactory validPassword. ].
	
	self 
		should: listingPurchases 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :exception |			self assert: RestInterface invalidUserErrorDescription equals: exception messageText.			].! !

!RestInterfaceTest methodsFor: 'testing' stamp: 'kamado tanjirou 11/11/2021 16:28:48'!
test08CanListPurchasesOfValidUser

	| interface clientId purchases clientPassword cartToBeCheckedOut cartNotToBeCheckedOut |
	
	interface _ RestInterface withAuthenticator: self withCatalogue: testObjectsFactory defaultCatalogue withClock: self withMp: self.
	clientId _ testObjectsFactory validUser.
	clientPassword _ testObjectsFactory validPassword .
	
	cartToBeCheckedOut _ interface createCartFor: clientId with: clientPassword.
	interface  addToCart: cartToBeCheckedOut bookIsbn: testObjectsFactory itemSellByTheStore bookQuantity: 1.
	interface 
		checkout: cartToBeCheckedOut 
		withCreditCardNumber: testObjectsFactory validCreditCardNumber 
		withExpiringDate: testObjectsFactory notExpiredDate 
		withOwnerName: testObjectsFactory validOwnerName.
	
	cartNotToBeCheckedOut _ interface createCartFor: clientId with: clientPassword.
	interface  addToCart: cartNotToBeCheckedOut bookIsbn: testObjectsFactory itemSellByTheStore bookQuantity: 1.
	
	purchases _ interface listPurchasesOf: clientId withPassword: clientPassword .
	
	self assert: purchases includes: testObjectsFactory itemSellByTheStore.
	self assert: 1 equals: purchases size.! !


!RestInterfaceTest methodsFor: 'setup' stamp: 'kamado tanjirou 11/11/2021 16:49:23'!
setUp

	testObjectsFactory _ StoreTestObjectsFactory new.
	shouldAuthenticate _ [:aUser :aPassword | true ].
	actualTime _ DateAndTime now.
	debitBehavior := [ :anAmount :aCreditCard | ].! !


!RestInterfaceTest methodsFor: 'authenticator protocol' stamp: 'jpm 11/4/2021 20:21:11'!
authenticate: aUser with: aPassword 
	
	^shouldAuthenticate value: aUser value: aPassword! !

!RestInterfaceTest methodsFor: 'authenticator protocol' stamp: 'kakashi 11/9/2021 00:12:32'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!RestInterfaceTest methodsFor: 'clock protocol' stamp: 'kamado tanjirou 11/11/2021 16:49:23'!
now
	^actualTime.! !


!RestInterfaceTest methodsFor: 'assertions' stamp: 'kamado tanjirou 11/11/2021 16:46:36'!
shouldRaiseExpiredCartErrorWhen: operatingWithExpiredCart 
	
	self should: operatingWithExpiredCart
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :exception | 			self assert: RestInterface expiredCartErrorDescription equals: exception messageText.].
		! !

!RestInterfaceTest methodsFor: 'assertions' stamp: 'kamado tanjirou 11/11/2021 16:40:18'!
shouldRaiseUnregisteredCartErrorWhen: operatingWithUnregisteredCart 
	
	self should: operatingWithUnregisteredCart 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :exception |self assert: RestInterface unregisteredCartErrorDescription equals: exception messageText.].! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:06'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 19:09'!
total

	^ items sum: [ :anItem | catalog at: anItem ]! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'accessing' stamp: 'lg 11/10/2021 22:08:08'!
listProductsOn: aBag 
	
	aBag addAll: 	items.
	^ aBag.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:08'!
calculateTotal

	total := cart total.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'lg 11/10/2021 22:07:09'!
createSale

	^ Sale of: total buying: (cart listProductsOn: Bag new).
! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
debitTotal

	merchantProcessor debit: total from: creditCard.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
registerSale

	salesBook add: self createSale! !


!Cashier methodsFor: 'checkout' stamp: 'HernanWilkinson 6/17/2013 19:06'!
checkOut

	self calculateTotal.
	self debitTotal.
	self registerSale.

	^ total! !


!Cashier methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:53'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:22'!
assertIsNotEmpty: aCart 
	
	aCart isEmpty ifTrue: [self error: self cartCanNotBeEmptyErrorMessage ]! !

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:23'!
assertIsNotExpired: aCreditCard on: aDate
	
	(aCreditCard isExpiredOn: aDate) ifTrue: [ self error: self canNotChargeAnExpiredCreditCardErrorMessage ]! !


!Cashier class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:51'!
toCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook
	
	self assertIsNotEmpty: aCart.
	self assertIsNotExpired: aCreditCard on: aDate.
	
	^self new initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook! !


!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 18:21'!
canNotChargeAnExpiredCreditCardErrorMessage
	
	^'Can not charge an expired credit card'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:56'!
cartCanNotBeEmptyErrorMessage
	
	^'Can not check out an empty cart'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 19:02'!
creditCardHasNoCreditErrorMessage
	
	^'Credit card has no credit'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 18:39'!
isExpiredOn: aDate 
	
	^expiration start < (Month month: aDate monthIndex year: aDate yearNumber) start ! !


!CreditCard methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:38'!
initializeExpiringOn: aMonth 
	
	expiration := aMonth ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:38'!
expiringOn: aMonth 
	
	^self new initializeExpiringOn: aMonth! !


!classDefinition: #RestInterface category: 'TusLibros'!
Object subclass: #RestInterface
	instanceVariableNames: 'authenticator carts catalogue clock merchantProcessor clientSalesbooks'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!RestInterface methodsFor: 'interface protocol' stamp: 'lg 11/10/2021 21:00:42'!
addToCart: aCartId bookIsbn: aBookIsbn bookQuantity: aBookQuantity

	| cart |

	cart _ self cartFromId: aCartId.
	
	cart add: aBookQuantity of: aBookIsbn.
	
	
	! !

!RestInterface methodsFor: 'interface protocol' stamp: 'lg 11/10/2021 21:48:39'!
checkout: aCartId withCreditCardNumber: aCreditCardNumber withExpiringDate: anExpiringDate withOwnerName: aName 
	
	| cart cashier  salebook |

	cart _ self cartFromId: aCartId.
	
	salebook _ clientSalesbooks at: cart owner. 

	cashier _ Cashier toCheckout: cart 
			charging: (CreditCard expiringOn: anExpiringDate) 
			throught: merchantProcessor 
			on: DateAndTime now 
			registeringOn: salebook.
	cashier checkOut .
			
	^ salebook last.! !

!RestInterface methodsFor: 'interface protocol' stamp: 'lg 11/10/2021 22:13:20'!
createCartFor: aUser with: aPassword
	
	self assertValidUser: aUser with: aPassword.
	
	self registerSalesbookFor: aUser.
	
	^ self createNewCartFor: aUser.! !

!RestInterface methodsFor: 'interface protocol' stamp: 'lg 11/10/2021 21:00:49'!
listCart: aCartId

	| products cart |
	
	cart _ self cartFromId: aCartId.
	
	products _ Bag new.
	cart listProductsOn: products.
	
	^products.! !

!RestInterface methodsFor: 'interface protocol' stamp: 'lg 11/10/2021 21:54:23'!
listPurchasesOf: aUser withPassword: aPassword 
	
	self assertValidUser: aUser with: aPassword.
	
	^ (clientSalesbooks at: aUser) inject: Bag new addingAll: [ :aSale | aSale products ].
	! !


!RestInterface methodsFor: 'operating with cart id' stamp: 'lg 11/10/2021 19:42:02'!
cartFromId: aCartId

	self assertRegisteredCart: aCartId.
	self assertCartIsNotExpired: aCartId.

	^ carts at: aCartId.! !

!RestInterface methodsFor: 'operating with cart id' stamp: 'lg 11/10/2021 22:26:38'!
createNewCartFor: aUser	
	| newCart newCartId |
	
	newCartId _ self newCartId.
	newCart _ Cart acceptingItemsOf: catalogue.
	
	carts add: newCartId->(InterfaceCart for: newCart withClock: clock ownedBy: aUser).
	
	^ newCartId.! !

!RestInterface methodsFor: 'operating with cart id' stamp: 'lg 11/10/2021 20:47:46'!
isExpiredCartId: aCartId

	| cartLastTimeUsed |
	
	cartLastTimeUsed _ (carts at: aCartId) lastTimeUsed.
	
	^ clock now - cartLastTimeUsed  > (Duration minutes: 30).! !

!RestInterface methodsFor: 'operating with cart id' stamp: 'lg 11/10/2021 19:44:27'!
isRegisteredCartId: aCartId

	^ carts includesKey: aCartId! !

!RestInterface methodsFor: 'operating with cart id' stamp: 'lg 11/10/2021 19:29:01'!
newCartId

	^ carts size! !

!RestInterface methodsFor: 'operating with cart id' stamp: 'lg 11/10/2021 21:51:48'!
registerSalesbookFor: aUser

	clientSalesbooks at: aUser 
		ifAbsent: [ clientSalesbooks add: aUser->OrderedCollection new. ].! !


!RestInterface methodsFor: 'assertions' stamp: 'kamado tanjirou 11/11/2021 16:46:36'!
assertCartIsNotExpired: aCartId

	^ (self isExpiredCartId: aCartId) ifTrue: [ self error: self class expiredCartErrorDescription. ].! !

!RestInterface methodsFor: 'assertions' stamp: 'kamado tanjirou 11/11/2021 16:40:18'!
assertRegisteredCart: aCartId

	^ (self isRegisteredCartId: aCartId) ifFalse: [ self error: self class unregisteredCartErrorDescription. ]! !

!RestInterface methodsFor: 'assertions' stamp: 'lg 11/10/2021 19:27:16'!
assertValidUser: aUser with: aPassword 

	^ (authenticator authenticate: aUser with: aPassword) ifFalse: [ self error: RestInterface invalidUserErrorDescription. ]! !


!RestInterface methodsFor: 'initialization' stamp: 'lg 11/10/2021 21:48:26'!
initializeWithAuthenticator: anAuthenticator withCatalogue: aCatalogue withClock: aClock withMp: aMerchantProcessor    
	
	authenticator := anAuthenticator.
	catalogue := aCatalogue.
	clock := aClock.
	merchantProcessor := aMerchantProcessor.
	
	carts := Dictionary new.
	
	clientSalesbooks := Dictionary new.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RestInterface class' category: 'TusLibros'!
RestInterface class
	instanceVariableNames: ''!

!RestInterface class methodsFor: 'errors' stamp: 'kamado tanjirou 11/11/2021 16:46:36'!
expiredCartErrorDescription
	
	^'Cart has already been destroyed'! !

!RestInterface class methodsFor: 'errors' stamp: 'jpm 11/4/2021 20:11:59'!
invalidUserErrorDescription

	^'Can not perform operation with invalid user!!!!!!'! !

!RestInterface class methodsFor: 'errors' stamp: 'kamado tanjirou 11/11/2021 16:40:18'!
unregisteredCartErrorDescription
	
	^'Cart ID not found!!!!!!'! !


!RestInterface class methodsFor: 'instance creation' stamp: 'kakashi 11/9/2021 00:14:13'!
withAuthenticator: anAuthenticator withCatalogue: aCatalogue withClock: aClock withMp: aMerchantProcessor  
	
	^self new initializeWithAuthenticator: anAuthenticator withCatalogue: aCatalogue withClock: aClock withMp: aMerchantProcessor ! !


!classDefinition: #Sale category: 'TusLibros'!
Object subclass: #Sale
	instanceVariableNames: 'total products'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 18:48'!
total
	
	^ total! !


!Sale methodsFor: 'initialization' stamp: 'lg 11/10/2021 22:10:41'!
initializeTotal: aTotal buying: boughtProducts 

	total _ aTotal.
	products _ boughtProducts.! !


!Sale methodsFor: 'as yet unclassified' stamp: 'lg 11/10/2021 21:59:06'!
products
	
	^ products.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: 'TusLibros'!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'lg 11/10/2021 22:04:14'!
of: aTotal buying: boughtProducts 

	"should assert total is not negative or 0!!"
	^self new initializeTotal: aTotal buying: boughtProducts ! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStorePrice
	
	^10! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'jpm 11/4/2021 20:41:45'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalogue! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'jpm 11/4/2021 20:41:45'!
defaultCatalogue
	
	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice;
		yourself ! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:52:15'!
expiredCreditCard
	
	^CreditCard expiringOn: (self expiredDate)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:52:15'!
expiredDate

	^ Month month: today monthIndex year: today yearNumber - 1! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:51:04'!
invalidCreditCardNumber
	
	^11112222! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:50:32'!
invalidOwnerName
	
	^''! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:52:00'!
notExpiredCreditCard
	
	^CreditCard expiringOn: (self notExpiredDate)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:52:00'!
notExpiredDate

	^ Month month: today monthIndex year: today yearNumber + 1! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:50:57'!
validCreditCardNumber
	
	^1111222233334444! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'jpm 11/4/2021 21:50:18'!
validOwnerName
	
	^'Fermin'! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'kakashi 11/10/2021 17:41:08'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !


!StoreTestObjectsFactory methodsFor: 'users and passwords' stamp: 'jpm 11/4/2021 20:12:57'!
invalidPassword
	^'invalid password'! !

!StoreTestObjectsFactory methodsFor: 'users and passwords' stamp: 'jpm 11/4/2021 20:04:25'!
invalidUser
	^'invalid user'! !

!StoreTestObjectsFactory methodsFor: 'users and passwords' stamp: 'jpm 11/4/2021 20:09:07'!
validPassword
	^'valid password'! !

!StoreTestObjectsFactory methodsFor: 'users and passwords' stamp: 'jpm 11/4/2021 20:12:40'!
validUser
	^'valid user'! !


!classDefinition: #InterfaceCart category: 'TusLibros'!
ProtoObject subclass: #InterfaceCart
	instanceVariableNames: 'cart clock lastTimeUsed owner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!InterfaceCart methodsFor: 'initialization' stamp: 'lg 11/10/2021 21:58:31'!
initializeFor: aCart withClock: aClock ownedBy: aUser  
	
	cart _ aCart.
	clock _ aClock.
	owner _ aUser.
	
	lastTimeUsed _ clock now.
	
	! !


!InterfaceCart methodsFor: 'cart context' stamp: 'lg 11/10/2021 22:18:51'!
lastTimeUsed
	
	^ lastTimeUsed.! !

!InterfaceCart methodsFor: 'cart context' stamp: 'lg 11/10/2021 21:54:52'!
owner
	
	^ owner.! !


!InterfaceCart methodsFor: 'cart protocol' stamp: 'lg 11/10/2021 22:18:07'!
doesNotUnderstand: aSelector

	lastTimeUsed _ clock now.
	^ aSelector sendTo: cart.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'InterfaceCart class' category: 'TusLibros'!
InterfaceCart class
	instanceVariableNames: ''!

!InterfaceCart class methodsFor: 'instance creation' stamp: 'lg 11/10/2021 21:58:03'!
for: aCart withClock: aClock ownedBy: aUser  

	^self new initializeFor: aCart withClock: aClock ownedBy: aUser ! !
