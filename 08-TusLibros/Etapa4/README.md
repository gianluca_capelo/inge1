## Levantar el webserver (puerto 8080)

```
testsObjects := StoreTestObjectsFactory new. 
server := testsObjects defaultRestInterface.
```

## Destruir el webserver

```
server destroy.
```

## Abrir la ventana de login

```
TusLibrosClientWindow open.
```

## Datos de login

```
user: user
password: password
```

## Generar tarjetas inválidas

```
testsObjects shouldDebit: [ :anAmount :aCreditCard | self error: 'Fondos insuficientes!!!'].

testsObjects shouldDebit: [ :anAmount :aCreditCard | self error: 'Tarjeta robada!!!'].

testsObjects shouldDebit: [ :anAmount :aCreditCard | self error: 'Tarjeta expirada!!!'].
```