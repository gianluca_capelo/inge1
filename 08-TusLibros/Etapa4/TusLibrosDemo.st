!classDefinition: #TusLibrosClientWindow category: 'TusLibrosDemo'!
SystemWindow subclass: #TusLibrosClientWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosDemo'!

!TusLibrosClientWindow methodsFor: 'initialization' stamp: 'EC 11/28/2021 19:55:40'!
buildMorphicWindow

	self registerEvents.
		
	self layoutMorph beColumn;
		separation: 15;
		axisEdgeWeight: 0;
		addMorph: self buildLoginBox.! !

!TusLibrosClientWindow methodsFor: 'initialization' stamp: 'EC 11/28/2021 19:29:38'!
initialExtent

	^`540@400` * Preferences standardCodeFont lineSpacing // 14! !

!TusLibrosClientWindow methodsFor: 'initialization' stamp: 'EC 11/28/2021 20:37:08'!
registerEvents

	self model 
		when: #logInSuccesful 
		send: #displayShoppingWindow 
		to: self;
		 
		when: #logInUnsuccessful 
		send: #notifyLogInError 
		to: self;
		
		when: #checkOutSuccesful: 
		send: 	#displayTicketWithisbns:withPrices:withQuantities:withTicketTotal:  
		to: self;
		
		when: #checkOutUnsuccesful
		send: #notifyCheckOutError:
		to: self.! !


!TusLibrosClientWindow methodsFor: 'GUI ticket' stamp: 'EC 11/28/2021 20:59:58'!
displayTicketWithisbns: anArrayOfIsbns withPrices: anArrayOfPrices withQuantities: anArrayOfQuantities withTicketTotal: aTicketTotal 


	| ticketIsbns ticketPrices ticketIsbnsPane ticketPricesPane purchasesRow  backColor labelBackground ticketQuantities ticketQuantitiesPane ticketMorph |
	
	backColor := self textBackgroundColor.	
	labelBackground := Theme current background.
	
	ticketIsbns := PluggableListMorph
		model: anArrayOfIsbns 
		listGetter: #yourself
		indexGetter: nil
		indexSetter: nil.
	ticketIsbns color: backColor;
			   hideScrollBarsIndefinitely.
	ticketIsbnsPane := LayoutMorph newColumn
		color: labelBackground;
		addMorph: (WidgetMorph new noBorder color: `Color transparent`) fixedHeight: 4;
		addMorphKeepMorphHeight: (LabelMorph new contents: ' Isbn');
		addMorphUseAll: ticketIsbns.

	ticketPrices := PluggableListMorph
		model: anArrayOfPrices 
		listGetter: #yourself
		indexGetter: nil
		indexSetter: nil.
	ticketPrices color: backColor.
	ticketPricesPane := LayoutMorph newColumn
		color: labelBackground;
		addMorph: (WidgetMorph new noBorder color: `Color transparent`) fixedHeight: 4;
		addMorphKeepMorphHeight: (LabelMorph new contents: 'Price');
		addMorphUseAll: ticketPrices.
		
	ticketQuantities := PluggableListMorph
		model: anArrayOfQuantities 
		listGetter: #yourself
		indexGetter: nil
		indexSetter: nil.
	ticketQuantities color: backColor.
	ticketQuantitiesPane := LayoutMorph newColumn
		color: labelBackground;
		addMorph: (WidgetMorph new noBorder color: `Color transparent`) fixedHeight: 4;
		addMorphKeepMorphHeight: (LabelMorph new contents: 'Quantity');
		addMorphUseAll: ticketQuantities.	
	

	purchasesRow := LayoutMorph newRow.
	purchasesRow
		addMorph: ticketIsbnsPane proportionalWidth: 0.33;
		addAdjusterAndMorph: ticketPricesPane proportionalWidth: 0.33;
		addAdjusterAndMorph: ticketQuantitiesPane proportionalWidth: 0.33.
		
		
	ticketIsbns rightSibling: ticketPrices; scrollSiblings: true.
	ticketPrices leftSibling: ticketIsbns; rightSibling: ticketQuantities ;scrollSiblings: true.
	ticketQuantities leftSibling: ticketPrices .
	
	ticketMorph _ (LayoutMorph newColumn)
		separation: 10;
		addMorph: purchasesRow proportionalHeight: 0.85;
		addMorph: (LabelMorph contents: 'Total de la compra: $', aTicketTotal) proportionalHeight: 0.1.
	
	(SystemWindow new) 
		setLabel: 'Ticket';		
		addMorph:  ticketMorph;
		openInWorld.
		
	self confirmContinueShopping.! !


!TusLibrosClientWindow methodsFor: 'GUI auxiliars' stamp: 'EC 11/29/2021 16:49:01'!
buildTableUpdatingWith: listGetters labels: labels indexGetter: indexGetter indexSetter: indexSetter
	
	| row  backColor labelBackground pluggableLists panes |
	
	backColor := self textBackgroundColor.	
	labelBackground := Theme current background.
	
	pluggableLists _  listGetters collect: [ :listGetter | 
			(PluggableListMorph
				model: model 
				listGetter: listGetter 
				indexGetter: indexGetter 
				indexSetter: indexSetter)
					color: backColor.
			].
	
	pluggableLists allButLastDo: [ :pluggableList | pluggableList hideScrollBarsIndefinitely. ].
	
	panes _ OrderedCollection new.
	1 to: pluggableLists size do: [ :index |
			panes add: (LayoutMorph newColumn
				color: labelBackground;
				addMorph: (WidgetMorph new noBorder color: `Color transparent`) fixedHeight: 4;
				addMorphKeepMorphHeight: (LabelMorph new contents: (labels at: index) );
				addMorphUseAll: (pluggableLists at: index)).
		 ].

	row := LayoutMorph newRow.
	
	row addMorph: panes first.
	2 to: panes size do: [ :index | 
			row addAdjusterAndMorph: (panes at: index) proportionalWidth: 1/panes size.
		].	
	
	pluggableLists first rightSibling: pluggableLists second; scrollSiblings: true.
	2 to: panes size-1 do: [ :index | 
			(pluggableLists at: index) 
				leftSibling: (pluggableLists at: index-1) 
				rightSibling: (pluggableLists at: index+1);
				scrollSiblings: true.
				
		].	
	pluggableLists last leftSibling: pluggableLists penultimate; scrollSiblings: true.

	^ row.! !


!TusLibrosClientWindow methodsFor: 'GUI purchase history' stamp: 'EC 11/29/2021 15:54:02'!
buildPurchaseHistoryPane
	
	^ (LayoutMorph newColumn)
		addMorph: (LabelMorph contents: 'Purchases') proportionalHeight: 0.1;
		addMorph: self buildPurchasesHistoryContentsPane proportionalHeight: 0.8;
		addMorph: self buildTotalBox proportionalHeight: 0.1.! !

!TusLibrosClientWindow methodsFor: 'GUI purchase history' stamp: 'EC 11/29/2021 16:49:01'!
buildPurchasesHistoryContentsPane
	
	^ self 
		buildTableUpdatingWith: { #purchasedBooks. #purchasedPrices. #purchasedQuantities }
		labels:  { 'Isbn'. 'Price'. 'Quiantity' }
		indexGetter:  nil
		indexSetter: nil.
		! !

!TusLibrosClientWindow methodsFor: 'GUI purchase history' stamp: 'EC 11/29/2021 16:51:39'!
buildTotalBox
	
	| totalTextBox |
	
	totalTextBox := TextModelMorph textProvider: self model textGetter: #total textSetter: nil. 
	totalTextBox disableEditing .
	totalTextBox morphWidth: 20; morphHeight: 20.

	^ LayoutMorph newRow
		addMorph: (LabelMorph contents: 'Historical total: ');
		addMorph: totalTextBox.! !


!TusLibrosClientWindow methodsFor: 'GUI cart' stamp: 'EC 11/29/2021 16:49:01'!
buildCartContentsPane

	^ self 
		buildTableUpdatingWith:  { #cartBooks. #cartQuantities }
		labels:  { 'Isbn'. 'Quantity' }
		indexGetter:  #cartSelectionIndex
		indexSetter: #cartSelectionIndex:.
		
		
		
		! !

!TusLibrosClientWindow methodsFor: 'GUI cart' stamp: 'EC 11/29/2021 15:52:45'!
buildCartPane
	
	^ (LayoutMorph newColumn)
		addMorph: (LabelMorph contents: 'Cart') proportionalHeight: 0.1;
		addMorph: self buildCartContentsPane proportionalHeight: 0.7;
		addMorph:  self buildRemoveFromCartButton proportionalHeight: 0.1;
		addMorph: self buildCheckOutButton proportionalHeight: 0.1.
		
		
		
		
		! !

!TusLibrosClientWindow methodsFor: 'GUI cart' stamp: 'EC 11/29/2021 15:41:30'!
buildCheckOutButton

	^ PluggableButtonMorph 
		model: self model
		stateGetter: nil
		action: #checkOutCart 
		label: 'Check out cart'! !

!TusLibrosClientWindow methodsFor: 'GUI cart' stamp: 'EC 11/29/2021 15:42:10'!
buildRemoveFromCartButton

	^ PluggableButtonMorph 
		model: self model
		stateGetter: nil
		action: #removeFromCart 
		label: 'Remove from cart'! !


!TusLibrosClientWindow methodsFor: 'GUI catalogue' stamp: 'EC 11/29/2021 15:35:27'!
buildAddToCartButton

	^ PluggableButtonMorph 
		model: self model
		stateGetter: nil
		action: #addToCart 
		label: 'Add to cart'! !

!TusLibrosClientWindow methodsFor: 'GUI catalogue' stamp: 'EC 11/29/2021 16:49:01'!
buildCatalogueContentsPane
	
	^ self 
		buildTableUpdatingWith: { #catalogueBooks. #cataloguePrices } 
		labels: { 'Isbn'. 'Price' } 
		indexGetter: #catalogueSelectionIndex 
		indexSetter: #catalogueSelectionIndex:.! !

!TusLibrosClientWindow methodsFor: 'GUI catalogue' stamp: 'EC 11/29/2021 15:52:05'!
buildCataloguePane

	^ (LayoutMorph newColumn)
		addMorph: (LabelMorph contents: 'Catalogue') proportionalHeight: 0.1;
		addMorph: self buildCatalogueContentsPane proportionalHeight: 0.8;
		addMorph: self buildAddToCartButton proportionalHeight: 0.1.! !


!TusLibrosClientWindow methodsFor: 'GUI shopping' stamp: 'EC 11/29/2021 15:53:43'!
buildShoppingPane

	^ (LayoutMorph newRow)
		addMorph: self buildCataloguePane proportionalWidth: 0.33;
		addAdjusterAndMorph: self buildCartPane proportionalWidth: 0.33;
		addAdjusterAndMorph: self buildPurchaseHistoryPane proportionalWidth: 0.33.
! !

!TusLibrosClientWindow methodsFor: 'GUI shopping' stamp: 'EC 11/29/2021 15:52:20'!
displayShoppingWindow

	self layoutMorph 
		replaceSubmorph: self layoutMorph submorphs first 
		by: self buildShoppingPane.
		
 	self openInWorld.! !


!TusLibrosClientWindow methodsFor: 'user notification' stamp: 'EC 11/29/2021 14:12:50'!
confirmContinueShopping

	(self confirm: 'Stop shopping and log out?') 
		ifTrue: [ self delete. ].! !

!TusLibrosClientWindow methodsFor: 'user notification' stamp: 'EC 11/28/2021 20:29:42'!
notifyCheckOutError: aReasonForFailure
	
	self notifyUserWith: 'Failed to check out: ' , aReasonForFailure.

	
	! !

!TusLibrosClientWindow methodsFor: 'user notification' stamp: 'EC 11/28/2021 19:32:08'!
notifyLogInError
	
	self notifyUserWith: 'Login error!!'.

	
	! !

!TusLibrosClientWindow methodsFor: 'user notification' stamp: 'EC 11/28/2021 19:32:35'!
okToChange

	^ true.! !


!TusLibrosClientWindow methodsFor: 'GUI log in' stamp: 'HW 11/26/2021 18:43:25'!
buildCreateCartButton

	^ PluggableButtonMorph 
		model: self model
		stateGetter: nil
		action: #createCart 
		label: 'Login'.! !

!TusLibrosClientWindow methodsFor: 'GUI log in' stamp: 'AT 11/22/2021 19:56:00'!
buildLoginBox
	
	| loginBoxLayoutMorph |
		
	loginBoxLayoutMorph := LayoutMorph newColumn.
	loginBoxLayoutMorph separation: 25;
	axisEdgeWeight: 0.5;
	addMorph: self buildLoginUserBox;
	addMorph: self buildLoginPasswordBox;
	addMorph: self buildCreateCartButton.
	
	^loginBoxLayoutMorph.! !

!TusLibrosClientWindow methodsFor: 'GUI log in' stamp: 'EC 11/29/2021 15:15:20'!
buildLoginPasswordBox
	
	| passwordTextBox loginPasswordBoxLayoutMorph |
	
	passwordTextBox := TextModelMorph textProvider: self model textGetter: nil textSetter: #password:. 
	passwordTextBox innerTextMorph setProperty: #keyStroke: toValue: [ :key | passwordTextBox innerTextMorph acceptContents ] .
	passwordTextBox  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 300. 

		
	loginPasswordBoxLayoutMorph := LayoutMorph newColumn.
	loginPasswordBoxLayoutMorph separation: 15;
	axisEdgeWeight: 0.5;
	addMorph: (LabelMorph contents:'Password:');
	addMorph: passwordTextBox.
	
	^loginPasswordBoxLayoutMorph.! !

!TusLibrosClientWindow methodsFor: 'GUI log in' stamp: 'EC 11/29/2021 15:14:07'!
buildLoginUserBox
	
	| userTextBox loginUserBoxLayoutMorph |
	
	userTextBox := TextModelMorph textProvider: self model textGetter: nil textSetter: #userId:. 
	userTextBox innerTextMorph setProperty: #keyStroke: toValue: [ :key | userTextBox innerTextMorph acceptContents ] .
	userTextBox  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 300.

		
	loginUserBoxLayoutMorph := LayoutMorph newColumn.
	loginUserBoxLayoutMorph separation: 15;
	axisEdgeWeight: 0.5;
	addMorph: (LabelMorph contents:'User Id:');
	addMorph: userTextBox.

	^loginUserBoxLayoutMorph.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosClientWindow class' category: 'TusLibrosDemo'!
TusLibrosClientWindow class
	instanceVariableNames: ''!

!TusLibrosClientWindow class methodsFor: 'instance creation' stamp: 'jpjm 11/29/2021 17:37:02'!
example

	| testObjects server |

	testObjects _ StoreTestObjectsFactory new.
	server _ testObjects defaultRestInterface.

	TusLibrosClientWindow open.

	" Usuario y Contraseņia validos:
		User: user
		Password: password."

	"Para destruir el Web server: 
		server destroy."
		
	"Para modificar el comprtamiento del Merchant Processor:
		testsObjects shouldDebit: [ :anAmount :aCreditCard |  ... ]."! !

!TusLibrosClientWindow class methodsFor: 'instance creation' stamp: 'EC 11/28/2021 19:30:34'!
open

	^ self open: TusLibrosClientModel new label: 'Tus Libros Client'.! !


!classDefinition: #TusLibrosClientModel category: 'TusLibrosDemo'!
Object subclass: #TusLibrosClientModel
	instanceVariableNames: 'restInterface user password cartId catalogue catalogueSelectedIsbn cartSelectedIsbn total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosDemo'!

!TusLibrosClientModel methodsFor: 'shopping actions' stamp: 'EC 11/29/2021 15:07:35'!
addToCart
	
	(restInterface getCatalogue includesKey: catalogueSelectedIsbn ) ifTrue: [ 
		restInterface addToCart: cartId bookIsbn: catalogueSelectedIsbn amount: 1.
	
		self 
			changed: #cartBooks;
			changed: #cartQuantities.
	]! !

!TusLibrosClientModel methodsFor: 'shopping actions' stamp: 'EC 11/29/2021 15:07:50'!
checkOutCart
	
	| ticket isbns ticketDict prices quantities |
	
	[ ticket _ restInterface checkOutCart: cartId. ]
		on: Error
		do: [ :anException | ^ self triggerEvent: #checkOutUnsuccesful with: anException messageText. ].
	
	ticketDict _ ticket at: #ticket.
	isbns _ (ticketDict) keys.
	prices _ ticketDict values collect: [:priceAndQuantity |
				priceAndQuantity at: 1.
				].
	quantities _ ticketDict values collect: [:priceAndQuantity |
				priceAndQuantity at: 2.
				].
	self 
		triggerEvent: #checkOutSuccesful: 
		withArguments: {
			isbns.
			prices.
			quantities.
		 	ticket at: #total}.
	
	cartId  _ restInterface createCartWithId: user password: password.
	
	self 
		changed: #purchasedBooks;
		changed: #purchasedPrices;
		changed: #purchasedQuantities;
		changed: #cartBooks;
		changed: #cartQuantities;
		changed: #total.
	
	 ! !

!TusLibrosClientModel methodsFor: 'shopping actions' stamp: 'EC 11/29/2021 15:27:49'!
createCart
	
	[ cartId _ restInterface createCartWithId: user password: password. ]
		on: Error
		do: [ ^ self triggerEvent: #logInUnsuccessful. ].
	
	self triggerEvent: #logInSuccesful.

! !

!TusLibrosClientModel methodsFor: 'shopping actions' stamp: 'EC 11/29/2021 15:07:36'!
removeFromCart

	((restInterface listCart: cartId) includesKey: cartSelectedIsbn ) ifTrue: [
		restInterface removeFrom: cartId bookIsbn: cartSelectedIsbn amount: 1.

		self 
			changed: #cartBooks;
			changed: #cartQuantities.	
	].! !


!TusLibrosClientModel methodsFor: 'catalogue accessing' stamp: 'EC 11/29/2021 15:25:09'!
catalogue
	
	catalogue ifNil: [ catalogue _ restInterface getCatalogue. ].
	^ catalogue.! !

!TusLibrosClientModel methodsFor: 'catalogue accessing' stamp: 'EC 11/29/2021 15:22:45'!
catalogueBooks
	
	^ self catalogue keys.! !

!TusLibrosClientModel methodsFor: 'catalogue accessing' stamp: 'EC 11/29/2021 15:26:19'!
cataloguePrices
	
	^ self catalogue values.! !

!TusLibrosClientModel methodsFor: 'catalogue accessing' stamp: 'EC 11/29/2021 15:06:19'!
catalogueSelectionIndex
	
	^ self catalogueBooks indexOf: catalogueSelectedIsbn.! !

!TusLibrosClientModel methodsFor: 'catalogue accessing' stamp: 'EC 11/29/2021 15:06:30'!
catalogueSelectionIndex: aSelectedIndex 
	
	catalogueSelectedIsbn := aSelectedIndex = 0 ifFalse: [self catalogueBooks at: aSelectedIndex .].
	
	self 
		changed: #catalogueBooks; 
		changed: #cataloguePrices.! !


!TusLibrosClientModel methodsFor: 'cart accessing' stamp: 'EC 11/29/2021 15:07:35'!
cartBooks
	^((restInterface listCart: cartId) keys).! !

!TusLibrosClientModel methodsFor: 'cart accessing' stamp: 'jpm 11/27/2021 18:04:20'!
cartQuantities
	
	^(restInterface listCart: cartId) values.! !

!TusLibrosClientModel methodsFor: 'cart accessing' stamp: 'EC 11/29/2021 15:07:36'!
cartSelectionIndex
	
	^ self cartBooks indexOf: cartSelectedIsbn.! !

!TusLibrosClientModel methodsFor: 'cart accessing' stamp: 'EC 11/29/2021 15:07:36'!
cartSelectionIndex: selectedIndex
	
	cartSelectedIsbn := selectedIndex = 0 ifFalse: [ self cartBooks at: selectedIndex].
	
	self 
		changed: #cartBooks; 
		changed: #cartQuantities.! !


!TusLibrosClientModel methodsFor: 'purchase history accessing' stamp: 'EC 11/29/2021 15:07:50'!
purchasedBooks
	
	^ (restInterface listpurchasesOf: user withPassword: password) keys .
	
! !

!TusLibrosClientModel methodsFor: 'purchase history accessing' stamp: 'RS 11/28/2021 16:21:51'!
purchasedPrices

	^ (restInterface listpurchasesOf: user withPassword: password) values .

	
	! !

!TusLibrosClientModel methodsFor: 'purchase history accessing' stamp: 'EC 11/29/2021 15:11:06'!
purchasedQuantities
	
	| quantities purchases |
	
	purchases _ restInterface listpurchasesOf: user withPassword: password .
	quantities _ self purchasedBooks collect: [:isbn | 
							(purchases at: isbn )/ (catalogue at: isbn)
							].
						
	^quantities ! !

!TusLibrosClientModel methodsFor: 'purchase history accessing' stamp: 'jpm 11/28/2021 17:33:09'!
total
	
	| purchases |
	purchases _ restInterface listpurchasesOf: user withPassword:  password .
	
	total _ (purchases values sum: [:price| price] ifEmpty: [0]) asString.
	
	^ total! !


!TusLibrosClientModel methodsFor: 'log in accessing' stamp: 'HW 11/26/2021 19:07:53'!
password: aPassword 
	
	password _ aPassword asString.! !

!TusLibrosClientModel methodsFor: 'log in accessing' stamp: 'HW 11/26/2021 19:08:03'!
userId: aUser 
	
	user _ aUser asString.! !


!TusLibrosClientModel methodsFor: 'initialization' stamp: 'EC 11/29/2021 15:14:53'!
initialize

	restInterface _ TusLibrosClientRestInterface new.
	
	total _ '0'.! !


!classDefinition: #TusLibrosClientRestInterface category: 'TusLibrosDemo'!
Object subclass: #TusLibrosClientRestInterface
	instanceVariableNames: 'port'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosDemo'!

!TusLibrosClientRestInterface methodsFor: 'http connection' stamp: 'AT 11/22/2021 20:26:27'!
port
	
	^port ifNil: [port _ 8080].! !

!TusLibrosClientRestInterface methodsFor: 'http connection' stamp: 'AT 11/22/2021 20:25:36'!
url
	^ 'http://localhost:', self port asString.! !


!TusLibrosClientRestInterface methodsFor: 'shopping actions' stamp: 'jpm 11/27/2021 17:38:35'!
addToCart: aCartId bookIsbn: aBookIsbn amount: bookQuantity 
	
	| resp |
	resp := WebClient 
		htmlSubmit: (self url,'/addToCart') 
		fields: {
			'cartId'->aCartId .
			'bookIsbn'->aBookIsbn .
			'bookQuantity'->bookQuantity 
			} 
		method: 'POST'.
		
	resp isSuccess ifFalse:[^self error: resp content].! !

!TusLibrosClientRestInterface methodsFor: 'shopping actions' stamp: 'EC 11/28/2021 20:28:46'!
checkOutCart: aCartId 
	
	
	| response |
	response _ WebClient 
		htmlSubmit: (self url,'/checkOutCart') 
		fields: {
			'cartId'->aCartId
			}
		method: 'GET'.

	response isSuccess 
		ifTrue: [^ (WebUtils jsonDecode: response content readStream). ] 
		ifFalse: [ self error: response content. ].! !

!TusLibrosClientRestInterface methodsFor: 'shopping actions' stamp: 'EC 11/26/2021 21:21:21'!
createCartWithId: aText password: aText2 
	
	|resp|
	
	resp _ WebClient 
		htmlSubmit: (self url,'/createCart' ) 
		fields: {
			'clientId'->aText .
			'password'->aText2.
			} 
		method: 'POST'.
		
	resp isSuccess 
		ifTrue: [^ (WebUtils jsonDecode: resp content readStream). ] 
		ifFalse: [ self error: 'Failed login!!!!!!'. ].! !

!TusLibrosClientRestInterface methodsFor: 'shopping actions' stamp: 'AT 11/22/2021 20:22:57'!
getCatalogue
	
	| resp |
	
	resp _ WebClient 
		htmlSubmit: (self url,'/catalog') 
		fields: {}
		method: 'GET'.
		
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: resp content readStream) asDictionary .] 
		ifFalse:[^self error: resp content].! !

!TusLibrosClientRestInterface methodsFor: 'shopping actions' stamp: 'jpm 11/27/2021 17:53:11'!
listCart: aCartId

	| resp |
	resp _ WebClient 
		htmlSubmit: (self url,'/listCart') 
		fields: {
			'cartId'->aCartId
		}
		method: 'GET'.
		
	resp isSuccess 
		ifTrue:[^(WebUtils jsonDecode: resp content readStream) asDictionary .] 
		ifFalse:[^self error: resp content].! !

!TusLibrosClientRestInterface methodsFor: 'shopping actions' stamp: 'RS 11/28/2021 15:26:07'!
listpurchasesOf: clientId withPassword: clientPassword 
	
	
	| response |
	response _ WebClient 
			htmlSubmit: (self url,'/listPurchases') 
			fields: {
				'clientId'->clientId.
				'password'->clientPassword.
			}
			method: 'POST'.
	
	response isSuccess 
		ifTrue: [ ^ (WebUtils jsonDecode: response content readStream ). ] 
		ifFalse: [ self error: 'Failed login!!!!!!'. ].
	
	! !

!TusLibrosClientRestInterface methodsFor: 'shopping actions' stamp: 'jpm 11/27/2021 19:28:05'!
removeFrom: aCartId bookIsbn: aBookIsbn amount: bookQuantity 
	
	| resp |
	resp := WebClient 
		htmlSubmit: (self url,'/removeFromCart') 
		fields: {
			'cartId'->aCartId .
			'bookIsbn'->aBookIsbn .
			'bookQuantity'->bookQuantity 
			} 
		method: 'POST'.
		
	resp isSuccess ifFalse:[^self error: resp content].! !
