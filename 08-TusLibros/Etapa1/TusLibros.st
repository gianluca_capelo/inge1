!classDefinition: #TusLibrosTest category: 'TusLibros'!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'testing' stamp: 'gc 10/29/2021 17:35:04'!
test01NewCartBeginsEmpty
	
	| aCart |
	
	aCart := self cart.
	
	self assert: aCart isEmpty.! !

!TusLibrosTest methodsFor: 'testing' stamp: 'gc 10/29/2021 18:23:28'!
test02CanAddABook
	
	| aCart aBook |
	
	aCart := self cart.
	aBook := self bookWithISBN: '2' .
	
	aCart add: aBook.

	self deny: aCart isEmpty.
	self assert: (aCart has: aBook).! !

!TusLibrosTest methodsFor: 'testing' stamp: 'gc 10/29/2021 18:23:28'!
test03EmptyCartHasNoBooks
	
	| aCart aBook |
	
	aCart := self cart.
	aBook := self bookWithISBN: '2'.
	
	self deny: (aCart has: aBook).! !

!TusLibrosTest methodsFor: 'testing' stamp: 'gc 10/29/2021 18:23:28'!
test05CanAddMultipleBooks
	
	| aCart aBook anotherBook|
	
	aCart := self cart.
	aBook := self bookWithISBN: '2'.
	anotherBook := self bookWithISBN: '3'.
	
	aCart add: aBook.
	aCart add: anotherBook.
		
	self assert: (aCart has: aBook).
	self assert: (aCart has: anotherBook).! !

!TusLibrosTest methodsFor: 'testing' stamp: 'gc 10/29/2021 18:04:26'!
test06CanAddABookMultipleTimes
	
	| aCart aBook |
	
	aCart := self cart.
	aBook := self bookWithISBN: '2'.

	aCart add: aBook withAmount: 3.
		
	self assert: (aCart has: aBook withAmount: 3).! !

!TusLibrosTest methodsFor: 'testing' stamp: 'gc 10/29/2021 18:03:38'!
test07CanNotAddAnUncataloguedBook
	
	| aCart aBook |
	
	aCart := self cart.
	aBook := self bookWithISBN: '5'.
	
	self 
		should: [ aCart add: aBook withAmount: 2] 
		raise: Error 
		withExceptionDo: [: anException |
			self assert: ShoppingCart canNotAddAnUncataloguedBookErrorDescription equals: anException messageText .
			self assert: aCart isEmpty.
			]! !

!TusLibrosTest methodsFor: 'testing' stamp: 'gc 10/29/2021 18:11:24'!
test08CanNotAddANonPositiveAmountOfABook
	
	| aBook aCart |
	
	aCart := self cart.
	aBook := self bookWithISBN: '3'.
	
	self 
		should: [ aCart add: aBook withAmount: -2]
		raise: Error 
		withExceptionDo: [: anException |
			self assert: ShoppingCart canNotAddANonPositiveAmountOfABook equals: anException messageText .
			self assert: aCart isEmpty.
			]! !


!TusLibrosTest methodsFor: 'test data' stamp: 'gc 10/29/2021 18:16:24'!
bookWithISBN: ISBN

	^ISBN.
! !

!TusLibrosTest methodsFor: 'test data' stamp: 'gc 10/29/2021 18:03:04'!
cart

	^ ShoppingCart for: self catalogue.! !

!TusLibrosTest methodsFor: 'test data' stamp: 'gc 10/29/2021 17:32:59'!
catalogue
	
	^Set with: (self bookWithISBN: '1') 
		with: (self bookWithISBN: '2')
		with: (self bookWithISBN: '3') 
		with:(self bookWithISBN: '4')
! !


!classDefinition: #ShoppingCart category: 'TusLibros'!
Object subclass: #ShoppingCart
	instanceVariableNames: 'books catalogue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!ShoppingCart methodsFor: 'testing' stamp: 'gc 10/29/2021 18:24:11'!
has: aBook 
	
	^books includes: aBook.! !

!ShoppingCart methodsFor: 'testing' stamp: 'gc 10/29/2021 18:04:26'!
has: aBook withAmount: aPossibleAmount 
	
	^ aPossibleAmount = (books occurrencesOf: aBook) ! !

!ShoppingCart methodsFor: 'testing' stamp: 'jpm 10/28/2021 21:24:08'!
isEmpty
	
	^books isEmpty.! !


!ShoppingCart methodsFor: 'adding' stamp: 'gc 10/29/2021 18:09:38'!
add: aBook 

	self add: aBook withAmount: 1.! !

!ShoppingCart methodsFor: 'adding' stamp: 'gc 10/29/2021 18:03:38'!
add: aBook withAmount: amountOfBooksToBeAdded 

	self assertBookIsInCatalogue: aBook. 
	self assertPositiveAmount: amountOfBooksToBeAdded .
	
	books add: aBook withOccurrences: amountOfBooksToBeAdded .! !



!ShoppingCart methodsFor: 'assertions' stamp: 'gc 10/29/2021 18:30:23'!
assertBookIsInCatalogue: aBook

	(catalogue includes: aBook) ifFalse: [self signalCanNotAddAnUncataloguedBookError ].! !

!ShoppingCart methodsFor: 'assertions' stamp: 'gc 10/29/2021 17:55:59'!
assertPositiveAmount: anUnvalidatedAmount

	(anUnvalidatedAmount > 0) ifFalse: [	self signalCanNotAddANonPositiveAmountOfABook.]! !


!ShoppingCart methodsFor: 'initialization' stamp: 'gc 10/29/2021 18:25:37'!
initializeFor: aCatalogue 
	
	catalogue := aCatalogue.
	books := Bag new.! !


!ShoppingCart methodsFor: 'signaling errors' stamp: 'gc 10/29/2021 17:55:59'!
signalCanNotAddANonPositiveAmountOfABook

	^ self error: self class canNotAddANonPositiveAmountOfABook! !

!ShoppingCart methodsFor: 'signaling errors' stamp: 'gc 10/29/2021 18:26:05'!
signalCanNotAddAnUncataloguedBookError

	^ self error: self class canNotAddAnUncataloguedBookErrorDescription! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingCart class' category: 'TusLibros'!
ShoppingCart class
	instanceVariableNames: ''!

!ShoppingCart class methodsFor: 'instance creation' stamp: 'gc 10/29/2021 18:25:37'!
for: aCatalogue 

	^self new initializeFor: aCatalogue ! !


!ShoppingCart class methodsFor: 'error description' stamp: 'gc 10/29/2021 18:29:54'!
canNotAddANonPositiveAmountOfABook
	
	^'The amount of books must be positive!!!!!!'! !

!ShoppingCart class methodsFor: 'error description' stamp: 'gc 10/29/2021 17:52:28'!
canNotAddAnUncataloguedBookErrorDescription
	
	^'Can only add books in the catalogue'.! !
